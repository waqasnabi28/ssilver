<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class IndexControllerCore extends FrontController
{
	public $php_self = 'index';

	/**
	 * Assign template vars related to page content
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		
		
 			 $smallslider1 = $this->getSmallSliderImages(0,4, true);
             $smallslider2 = $this->getSmallSliderImages(4,4, true);
			 
			 
			
			 
		$this->context->smarty->assign(array(
			'header_flag' => '1',
			'smallslider1' => $smallslider1,
			'smallslider2' => $smallslider2,
		));	
		$this->context->smarty->assign('HOOK_HOME', Hook::exec('displayHome'));
		$this->setTemplate(_PS_THEME_DIR_.'index.tpl');
	}
	
	
	  private function getSmallSliderImages($from, $to, $active = false) {
       
     
           $limit = ' LIMIT  '.$from.','.$to;
    
       
       if ($active == false){
           $active = "";
       }else{
           $active = "`is_active` = 1";
       }
      
       $response = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                     
                      SELECT `ad_id`, `shop_id`, `lang_id`, `is_active`, `position`,`url`, `caption`, `image`
                      FROM '._DB_PREFIX_.'autumn_ads
                      WHERE '. $active .'
                      ORDER BY `position` ASC'
                      .$limit
                      );
       return $response;
    }
	
}
