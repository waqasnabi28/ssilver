<?php

require(dirname(__FILE__) . '/config/settings.inc.php');
require(dirname(__FILE__) . '/config/config.inc.php');
$filename = "Cusmsexp.csv";
$path = 'inventory/customerimport/';
$csv_to_read = $path . $filename;
$start_time = time();
if(file_exists($csv_to_read) && ($handle = fopen($csv_to_read, "r")) !== FALSE)
{
    $i = 0;

    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {


        $manual_id_customer = trim($data[0]);
        $name1 = trim($data[1]);
        $address1Line1 = trim($data[2]);
        $address1Line2 = trim($data[3]);
        $city1 = trim($data[4]) == '' ? '-' : trim($data[4]);
        $state1 = trim($data[5]);
        $zip1 = trim($data[6]) == '' ? 0 : trim($data[6]);
        $ShiptoNbr = trim($data[7]);
        $name2 = trim($data[8]);
        $address2Line1 = trim($data[9]);
        $address2Line2 = trim($data[10]);
        $city2 = trim($data[11]) == '' ? '-' : trim($data[11]);
        $state2 = trim($data[12]);
        $zip2 = trim($data[13]) == '' ? 0 : trim($data[13]);
        $email = trim($data[14]);


        if($i > 0)
        {


            $lname1 = '';
            $fname1 = '';
            $lname2 = '';
            $fname2 = '';
            if($name1 != '')
            {
                $name1a = explode(' ', $name1);
                if(is_array($name1a))
                {
                    $lname1 = $name1a[0];
                    $fname1 = '';
                    for($k = 1; $k < count($name1a); $k++)
                        $fname1 .= " " . $name1a[$k];
                    $lname1 = trim($lname1);
                    $fname1 = trim($fname1);
                }
                $name1 = $lname1 . " " . $fname1;
            }
            if($state1 != '')
            {
                $state1_query = Db::getInstance()->getRow("SELECT * FROM ps_state WHERE iso_code = '" . $state1 . "'");
                $id_state1 = $state1_query['id_state'];
            }

            if($state2 != '')
            {
                $state2_query = Db::getInstance()->getRow("SELECT * FROM ps_state WHERE iso_code = '" . $state2 . "'");
                $id_state2 = $state2_query['id_state'];
            }

            if($name2 != '')
            {
                $name2a = explode(' ', $name2);
                if(is_array($name2a))
                {
                    $lname2 = $name2a[0];
                    $fname2 = '';
                    for($k = 1; $k < count($name2a); $k++)
                        $fname2 .= " " . $name2a[$k];
                    $lname2 = trim($lname2);
                    $fname2 = trim($fname2);
                }
                $name2 = $lname2 . " " . $fname2;
            }


            $query = Db::getInstance()->getRow('SELECT COUNT(`id_customer`) as total FROM ps_customer WHERE manual_id_customer = "' . $manual_id_customer . '" AND lastname = "' . $lname1 . '" AND firstname = "' . $fname1 . '" ORDER BY id_customer');
            $total_rows = $query['total'];

            $q1 = Db::getInstance()->getRow('SELECT * FROM ps_customer WHERE manual_id_customer = "' . $manual_id_customer . '" AND lastname = "' . $lname1 . '" AND firstname = "' . $fname1 . '" ORDER BY id_customer');

            if($total_rows > 0)
            {
                $id_customer = $q1['id_customer'];

                if($address1Line1 != '' || $address1Line2 != '')
                {
                    if(strpos($address1Line1, '"') !== false)
                        $address1Line1 = "'" . $address1Line1 . "'";
                    else
                        $address1Line1 = '"' . $address1Line1 . '"';
                    if(strpos($address1Line2, '"') !== false)
                        $address1Line2 = "'" . $address1Line2 . "'";
                    else
                        $address1Line2 = '"' . $address1Line2 . '"';
                    if(strpos($name1, '"') !== false)
                        $name1 = "'" . $name1 . "'";
                    else
                        $name1 = '"' . $name1 . '"';
                    if(strpos($city1, '"') !== false)
                        $city1 = "'" . $city1 . "'";
                    else
                        $city1 = '"' . $city1 . '"';
                    if(strpos($fname1, '"') !== false)
                        $fname1 = "'" . $fname1 . "'";
                    else
                        $fname1 = '"' . $fname1 . '"';
                    if(strpos($lname1, '"') !== false)
                        $lname1 = "'" . $lname1 . "'";
                    else
                        $lname1 = '"' . $lname1 . '"';


                    $q21 = Db::getInstance()->getRow("SELECT Count(*) as total FROM ps_address WHERE id_customer = '" . $id_customer . "' AND CONCAT(lastname, ' ', firstname) = " . $name1 . " AND address1 = " . $address1Line1 . " AND address2 = " . $address1Line2 . " AND city = " . $city1 . " AND id_state = '" . $id_state1 . "' AND postcode = '" . $zip1 . "'");

                    $total_rows_q21 = $q21['total'];

                    //$q21 = "SELECT * FROM ps_address WHERE id_customer = '".$id_customer."' AND CONCAT(lastname, ' ', firstname) = ".$name1." AND address1 = ".$address1Line1." AND address2 = ".$address1Line2." AND city = ".$city1." AND id_state = '".$id_state1."' AND postcode = '".$zip1."'";
// echo 'all the total rows'.$total_rows_q21; exit;
                    // $q22 = mysql_query($q21);
                    if($total_rows_q21 == 0)
                    {
//
                        $q3 = "INSERT INTO ps_address (id_state, id_customer, lastname, firstname, postcode, city, address1, address2) VALUES ('" . $id_state1 . "', '" . $id_customer . "', " . $lname1 . ", " . $fname1 . ", '" . $zip1 . "', " . $city1 . ", " . $address1Line1 . ", " . $address1Line2 . ")";
                        Db::getInstance()->execute($q3);
                    }
                }





                if($address2Line1 != '' || $address2Line2 != '')
                {
                    if(strpos($address2Line1, '"') !== false)
                        $address2Line1 = "'" . $address2Line1 . "'";
                    else
                        $address2Line1 = '"' . $address2Line1 . '"';
                    if(strpos($address2Line2, '"') !== false)
                        $address2Line2 = "'" . $address2Line2 . "'";
                    else
                        $address2Line2 = '"' . $address2Line2 . '"';
                    if(strpos($name2, '"') !== false)
                        $name2 = "'" . $name2 . "'";
                    else
                        $name2 = '"' . $name2 . '"';
                    if(strpos($city2, '"') !== false)
                        $city2 = "'" . $city2 . "'";
                    else
                        $city2 = '"' . $city2 . '"';
                    if(strpos($fname2, '"') !== false)
                        $fname2 = "'" . $fname2 . "'";
                    else
                        $fname2 = '"' . $fname2 . '"';
                    if(strpos($lname2, '"') !== false)
                        $lname2 = "'" . $lname2 . "'";
                    else
                        $lname2 = '"' . $lname2 . '"';
                    //if(strpos($address2Line2, '"') !== false || strpos($address2Line1, '"') !== false || strpos($name2, '"') !== false){
                    $q21 = Db::getInstance()->getRow("SELECT count(*) as total FROM ps_address WHERE id_customer = '" . $id_customer . "' AND CONCAT(lastname, ' ', firstname) = " . $name2 . " AND address1 = " . $address2Line1 . " AND address2 = " . $address2Line2 . " AND city = " . $city2 . " AND id_state = '" . $id_state2 . "' AND postcode = '" . $zip2 . "'");
                    $total_rows_q21 = $q21['total'];
                    if($total_rows_q21 == 0)
                    {
//                                            echo "<b>".$q21."</b>\t";
                        $q3 = "INSERT INTO ps_address (id_state, id_customer, lastname, firstname, postcode, city, address1, address2) VALUES ('" . $id_state2 . "', '" . $id_customer . "', " . $lname2 . ", " . $fname2 . ", '" . $zip2 . "', " . $city2 . ", " . $address2Line1 . ", " . $address2Line2 . ")";
                        Db::getInstance()->execute($q3);
                    }
                }
            }
            else
            {
                if(strpos($fname1, '"') !== false)
                    $fname1 = "'" . $fname1 . "'";
                else
                    $fname1 = '"' . $fname1 . '"';
                if(strpos($lname1, '"') !== false)
                    $lname1 = "'" . $lname1 . "'";
                else
                    $lname1 = '"' . $lname1 . '"';

                $q = 'INSERT INTO ps_customer (manual_id_customer, firstname, lastname, email) VALUES ("' . $manual_id_customer . '", ' . $fname1 . ', ' . $lname1 . ', "' . $email . '")';
                Db::getInstance()->execute($q);
                $id_customer = Db::getInstance()->Insert_ID();
                $q2 = 'INSERT INTO ps_customer_group (id_customer, id_group) VALUES (' . $id_customer . ', 1)';
                Db::getInstance()->execute($q2);
                if($address1Line1 != '' || $address1Line2 != '')
                {
                    if(strpos($address1Line1, '"') !== false)
                        $address1Line1 = "'" . $address1Line1 . "'";
                    else
                        $address1Line1 = '"' . $address1Line1 . '"';
                    if(strpos($address1Line2, '"') !== false)
                        $address1Line2 = "'" . $address1Line2 . "'";
                    else
                        $address1Line2 = '"' . $address1Line2 . '"';
                    if(strpos($city1, '"') !== false)
                        $city1 = "'" . $city1 . "'";
                    else
                        $city1 = '"' . $city1 . '"';
                    /* if(strpos($fname1, '"') !== false)
                      $fname1 = "'".$fname1."'";
                      else
                      $fname1 = '"'.$fname1.'"';
                      if(strpos($lname1, '"') !== false)
                      $lname1 = "'".$lname1."'";
                      else
                      $lname1 = '"'.$lname1.'"'; */
                    $q2_1 = Db::getInstance()->getRow("SELECT count(*) as total FROM ps_address WHERE id_customer = '" . $id_customer . "' AND CONCAT(lastname, ' ', firstname) = \"" . $name1 . "\" AND address1 = " . $address1Line1 . " AND address2 = " . $address1Line2 . " AND city = " . $city1 . " AND id_state = '" . $id_state1 . "' AND postcode = '" . $zip1 . "'");
                    $total_rows_q2_1 = $q2_1['total'];


                    //$q2_2 = mysql_query($q2_1);
                    if($total_rows_q2_1 == 0)
                    {

                        $q3 = 'INSERT INTO ps_address (id_state, id_customer, lastname, firstname, postcode, city, address1, address2) VALUES
                                        ("' . $id_state1 . '", "' . $id_customer . '", ' . $lname1 . ', ' . $fname1 . ', "' . $zip1 . '", ' . $city1 . ', ' . $address1Line1 . ', ' . $address1Line2 . ')';
                        Db::getInstance()->execute($q3);
                    }
                }
                if($address2Line1 != '' || $address2Line2 != '')
                {
                    if(strpos($address2Line1, '"') !== false)
                        $address2Line1 = "'" . $address2Line1 . "'";
                    else
                        $address2Line1 = '"' . $address2Line1 . '"';
                    if(strpos($address2Line2, '"') !== false)
                        $address2Line2 = "'" . $address2Line2 . "'";
                    else
                        $address2Line2 = '"' . $address2Line2 . '"';
                    if(strpos($city2, '"') !== false)
                        $city2 = "'" . $city2 . "'";
                    else
                        $city2 = '"' . $city2 . '"';



                    $name2 = "'" . $lname2 . " " . $fname2 . "'";

                    $q3_1 = Db::getInstance()->getRow("SELECT count(*) as total  FROM ps_address WHERE id_customer = '" . $id_customer . "' AND CONCAT(lastname, ' ', firstname) = " . $name2 . " AND address1 = " . $address2Line1 . " AND address2 = " . $address2Line2 . " AND city = " . $city2 . " AND id_state = '" . $id_state2 . "' AND postcode = '" . $zip2 . "'");

                    $total_rows_q3_1 = $q3_1;
                    if($total_rows_q3_1 == 0)
                    {
                        if(strpos($fname2, '"') !== false)
                            $fname2 = "'" . $fname2 . "'";
                        else
                            $fname2 = '"' . $fname2 . '"';
                        if(strpos($lname2, '"') !== false)
                            $lname2 = "'" . $lname2 . "'";
                        else
                            $lname2 = '"' . $lname2 . '"';
                        $q3 = 'INSERT INTO ps_address (id_state, id_customer, lastname, firstname, postcode, city, address1, address2) VALUES   ("' . $id_state2 . '", "' . $id_customer . '", ' . $lname2 . ', ' . $fname2 . ', "' . $zip2 . '", ' . $city2 . ', ' . $address2Line1 . ', ' . $address2Line2 . ')';
                        Db::getInstance()->execute($q3);
                    }
                }
            }

//            if(isset($Email) && !empty($Email))
//            {
//
//                $row = Db::getInstance()->getRow('SELECT COUNT(`id_customer`) as total FROM `'._DB_PREFIX_.'customer` WHERE `email` = "'.$Email.'"');
//
//                $total_rows = $row['total'];
//
//                if($total_rows < 1){
//
//                   Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'customer` (
//                                                `id_gender`,
//                                                `id_default_group`,
//                                                `manual_id_customer`,
//                                                `id_lang`,`id_risk`,
//                                                `firstname`,
//                                                `lastname`,
//                                                `email`
//                                                ) VALUES (
//                                                 0,
//                                                 3,
//                                                 "'.$manual_customer_id.'",
//                                                 1,
//                                                 0,
//                                                 "'.$fname1.'",
//                                                 "'.$lname1.'",
//                                                "'.$Email.'")'
//
//                                            );
//                }
//
//
//
//            }
        }

        if($i > 100)
        {
            exit;
        }
        echo $i . "<br>";
        $i++;
    }
}
echo "done:" . $timerStart . ":" . (time() - $start_time) . " s";
exit;
?>
