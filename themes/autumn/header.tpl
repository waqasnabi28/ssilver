{* 2012 - Autumn Prestashop Theme - Sercan YEMEN - www.withinpixels.com *}
<!DOCTYPE html>
<html lang="{$lang_iso}">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>{$meta_title|escape:'vhtmlall':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
<meta name="description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
    {if isset($meta_keywords) AND $meta_keywords}
<meta name="keywords" content="{$meta_keywords|escape:html:'UTF-8'}" />
{/if}
<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
{*Meta data for Facebook*}
<meta property="og:title" content="{$meta_title|escape:'vhtmlall':'UTF-8'}" />
{if isset($meta_description) AND $meta_description}
<meta property="og:description" content="{$meta_description|escape:html:'UTF-8'}" />
{/if}
    {if isset($page_name) && $page_name == "product" && isset($have_image) && $have_image}
    <meta property="og:image" content="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')}" />
    {/if}
    <script type="text/javascript">
        var baseDir = '{$content_dir|addslashes}';
        var baseUri = '{$base_uri|addslashes}';
        var static_token = '{$static_token|addslashes}';
        var token = '{$token|addslashes}';
        var priceDisplayPrecision = {$priceDisplayPrecision*$currency->decimals};
        var priceDisplayMethod = {$priceDisplay};
        var roundMode = {$roundMode};
    </script>
    {if isset($css_files)}
        {foreach from=$css_files key=css_uri item=media}
    <link href="{$css_uri}" rel="stylesheet" type="text/css" media="{$media}" />
    {/foreach}
    {/if}

    <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="{$css_dir}ie8.css" />
    <![endif]-->

    {if isset($js_files)}
        {foreach from=$js_files item=js_uri}
    <script type="text/javascript" src="{$js_uri}"></script>
    {/foreach}
    {/if}
    {$HOOK_HEADER}

    {*Theme Options*}
    {$color_changer}
    {$logo_width}
    </head>
    <body itemscope itemtype="http://schema.org/WebPage" {if isset($page_name)}id="{$page_name|escape:'htmlall':'UTF-8'}"{/if} class="{if $hide_left_column}hide-left-column{/if} {if $hide_right_column}hide-right-column{/if} {if $content_only} content_only {/if}">
{if !$content_only}
{if isset($restricted_country_mode) && $restricted_country_mode}
<div id="restricted-country">
      <p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>
    </div>
{/if}
<div id="wrapper" class="group">

<!-- <div id="header-fluid" class="fluid-bg">
        <div class="container">
         
            <div id="header" class="column full">
                <div id="header-logo">
                    <a href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}">
                        <img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'htmlall':'UTF-8'}" />
                    </a>
                </div>

                <div id="header-right">
                   
                </div>
            </div>
        </div>
    </div> -->
    
		{if (isset($header_flag) && !empty($header_flag)) }
                <div class="wrapperMain">
                      <div class="bannermaincont">
                    <div class="header">
                          <div class="logoMain"><a href="http://www.ssilver.com/DEV2" title="STEVE SILVER CO."><img src="http://www.ssilver.com/themes/prestashop/img/logo.png" alt="STEVE SILVER CO."></a></div>
                          <div class="nav_lt_bg"></div>
                          <!--?php
                /* 
                * To change this template, choose Tools | Templates
                * and open the template in the editor.
                */
                
                ?-->
                          <link rel="stylesheet" type="text/css" href="http://www.ssilver.com/themes/prestashop/img/../css/modules/blockmenulavalamp/blockmenulavalamp.css" media="screen">
                          <div class="nav_mid_bg">
                        <div class="navigation">
                              <div class="navigation"><a style="padding-left:40px;" class="selected" href="http://www.ssilver.com/index.php">Home</a></div>
                              <div class="navigation"><a href="http://www.ssilver.com/DEV2/cms.php?id_cms=4">About Us</a></div>
                              <div class="navigation"><a href="http://www.ssilver.com/DEV2/index.php?id_category=3&controller=category">Catalog</a></div>
                              <div class="navigation"><a href="#">Where To Buy</a></div>
                              <div class="navigation"><a href="#">Contact</a></div>
                            </div>
                        
                        <!-- Block mymodule --> 
                        <!-- /Block mymodule --> 
                        
                        <!-- Block search module TOP -->
                        
                        <div class="searchfiled">
                              <form method="get" action="http://www.ssilver.com/search.php" id="searchbox">
                            <input type="hidden" name="orderby" value="position">
                            <input type="hidden" name="orderway" value="desc">
                            <input name="search_query" type="text" id="search_query_top" onclick="this.value='';" value="Search..." autocomplete="off" class="ac_input" style="z-index: 100000;">
                          </form>
                            </div>
                      </div>
                          <!-- /Block search module TOP -->
                          <div class="nav_rt_bg"></div>
                        </div>
                  </div>
                    </div>
                <script type="text/javascript" src="http://www.ssilver.com/themes/prestashop/js/cycle.js"></script> 
                <script type="text/javascript">
                (function($) {
                
                    $(document).ready(function() {
                        $('#fW_Content').cycle({ 
                
                        fx:     'fade',
                
                        speed:  '1000',
                
                        timeout: 8000,
                
                        pager:  '#fW_Controls'
                
                    });
                    
                    $('#fW_Content1').cycle({ 
                
                        fx:     'fade',
                
                        speed:  '1000',
                
                        timeout: 8000,
                
                        pager:  '#fW_Controls1'
                
                    });
                });	
                    
                })(jQuery);
                </script>
                <div class="bottommain">
                      <div class="bottomincont">
                    <div class="bottomcontinner" style="height: 100px !important;">
                        {if isset($smallslider1) and !empty($smallslider1)}
                      <div class="box1">
                        <div id="fW_Controls"></div>
                        
                        
                        
                        <div id="fW_Content" style="position: relative;">
                           {foreach from=$smallslider1 item=media}
                    		<div class="slidercont1">
                       		<div class="slidercontent1"><a href="{$media.link}" class="slidercontent1">{$media.caption}</a></div>
                       		 <div class="sliderimg1"><img src="{$smarty.const._MODULE_DIR_}/autumnads/ads/{$media.image}" alt="" /></div>
                    		</div>
                		{/foreach}
                          
                          
                          
                             <!-- <div class="slidercont1">
                            <div class="slidercontent1"><a href="./category.php?id_category=314" class="slidercontent1">Browse New Arrivals</a></div>
                            <div class="sliderimg1"><img src="themes/autumn/img/7.jpg" alt=""></div>
                          </div>
                              <div class="slidercont1">
                            <div class="slidercontent1"><a href="./category.php?id_category=318" class="slidercontent1">Browse All Categories</a></div>
                            <div class="sliderimg1"><img src="themes/autumn/img/10.jpg" alt=""></div>
                          </div>
                              <div class="slidercont1">
                            <div class="slidercontent1"><a href="./category.php?id_category=318" class="slidercontent1">Browse All Categories</a></div>
                            <div class="sliderimg1"><img src="themes/autumn/img/9.jpg" alt=""></div>
                          </div>-->
                            </div>
                      </div>
                      
                        {/if}
                        
        				{if isset($smallslider2) and !empty($smallslider2)}
                          <div class="box2">
                        <div id="fW_Controls1"></div>
                        
                        
                        
                        <div id="fW_Content1" style="position: relative;">
                              {foreach from=$smallslider2 item=media}
                    <div class="slidercont2">
                        <div class="slidercontent2"><a href="{$media.url}" class="slidercontent2">{$media.caption}</a></div>
                        
                       
                        <div class="sliderimg2"><img src="{$smarty.const._MODULE_DIR_}/autumnads/ads/{$media.image}" alt="" /></div>
                    </div>
                {/foreach}
                          
                              <!--<div class="slidercont2">
                            <div class="slidercontent2"><a href="./category.php?id_category=6" class="slidercontent2">Occasional</a></div>
                            <div class="sliderimg2"><img src="themes/autumn/img/1.jpg" alt=""></div>
                          </div>
                              <div class="slidercont2">
                            <div class="slidercontent2"><a href="./category.php?id_category=8" class="slidercontent2">Home Office</a></div>
                            <div class="sliderimg2"><img src="themes/autumn/img/3.jpg" alt=""></div>
                          </div>
                              <div class="slidercont2">
                            <div class="slidercontent2"><a href="./category.php?id_category=317" class="slidercontent2">NEW Upholstery</a></div>
                            <div class="sliderimg2"><img src="themes/autumn/img/6.jpg" alt=""></div>
                          </div>-->
                            </div>
                      </div>
                          {/if}
                      
                          <div class="box3">
                        <p>JOIN THE CONVERSATION</p>
                        <div class="socialicon"><a href="http://www.facebook.com/SteveSilverCompany" target="_&quot;blank&quot;"><img src="http://www.ssilver.com/themes/prestashop/img/facebook_icon.png" alt=""></a>&nbsp;&nbsp;<a href="http://www.twitter.com/SteveSilverCo" target="_&quot;blank&quot;"><img src="http://www.ssilver.com/themes/prestashop/img/twitter_icon.png" alt=""></a>&nbsp;&nbsp;<a href="#"><img src="http://www.ssilver.com/themes/prestashop/img/google_icon.png" alt=""></a>&nbsp;&nbsp;<a href="#"><img src="http://www.ssilver.com/themes/prestashop/img/youtube_icon.png" alt=""></a>&nbsp;&nbsp;<a href="#"><img src="http://www.ssilver.com/themes/prestashop/img/rss_icon.png" alt=""></a><br>
                              <div class="fb-like fb_edge_widget_with_comment fb_iframe_widget" data-href="https://www.facebook.com/SteveSilverCompany" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" fb-xfbml-state="rendered"><span style="height: 20px; width: 76px;">
                                <iframe id="f2a707ed68" name="fede5fa1" scrolling="no" title="Like this content on Facebook." class="fb_ltr" src="http://www.facebook.com/plugins/like.php?api_key=&amp;channel_url=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D28%23cb%3Df92ba527%26domain%3Dwww.ssilver.com%26origin%3Dhttp%253A%252F%252Fwww.ssilver.com%252Ff2645e358%26relation%3Dparent.parent&amp;colorscheme=light&amp;extended_social_context=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FSteveSilverCompany&amp;layout=button_count&amp;locale=en_US&amp;node_type=link&amp;sdk=joey&amp;send=false&amp;show_faces=false&amp;width=450" style="border: none; overflow: hidden; height: 20px; width: 76px;"></iframe>
                                </span></div>
                            </div>
                      </div>
                        </div>
                    <div class="dealerlogin_img"><a href="authentication.php"><img src="http://www.ssilver.com/themes/prestashop/img/dealerlogin_img.png" alt="" style="width:110px;"></a></div>
                  </div>
                    </div>
            
		{else}

            <div id="header-fluid" class="fluid-bg">
                  <div class="container">
                <div id="header" class="column full">
                      <div id="header-logo"> <a href="{$base_dir}" title="{$shop_name|escape:'htmlall':'UTF-8'}"> <img class="logo" src="{$logo_url}" alt="{$shop_name|escape:'htmlall':'UTF-8'}" /> </a> </div>
                      <div id="header-right"> {$HOOK_TOP} </div>
                    </div>
              </div>
                </div> 
		{/if}
<div id="content-fluid" class="fluid-bg">
{if $page_name == "index"}

            {if isset($slide_config)} {* Show the image slider only homepage *} 
<script type="text/javascript">
                    $(window).load(function() {
                        var s_effect = {$slide_config.effect};
                        var s_animspeed = {$slide_config.anim_speed};
                        var s_pausetime = {$slide_config.pause_time};

                        $('#autumn-slider').nivoSlider({
                            effect: s_effect,
                            pauseTime: s_pausetime,
                            animSpeed: s_animspeed,
                            controlNav: false
                        });

                        $('#autumn-slider').touchwipe({
                            wipeLeft: function() {
                                $('a.nivo-nextNav').trigger('click');
                            },

                            wipeRight: function() {
                                $('a.nivo-prevNav').trigger('click');
                            },

                            preventDefaultEvents: false
                        });

                    });
                </script> 
{/if}

            {if isset($slides) && isset($page_name) && $page_name == "index"}
            <div id="autumn-slider" class="nivoSlider"> {foreach from=$slides item=slide}
      {if $slide.is_active == 1}
      
      {if $slide.url != ""}<a class="nivo-imageLink" href="{$slide.url}">{/if} <img src="{$smarty.const._MODULE_DIR_}/autumnslider/slides/{$slide.image}" /> {if $slide.url != ""}</a>{/if}
      
      {/if}
      {/foreach} </div>
{/if}

        {/if}
<div class="content container">
<div class="column {if $HOOK_LEFT_COLUMN}two-third{else}full{/if}">
<!-- Column --> 
{/if} 
