<?php 
/*
Company: Invortex Technologies
Developer: Imran Mushtaq
Description: Add product from cvs to database
 */
require(dirname(__FILE__).'/config/settings.inc.php');
require(dirname(__FILE__).'/config/config.inc.php');
$filename = "ipad_inventory.csv"; //Cvs file name
$path = 'inventory/data/'; //cvs file name location
$csv_to_read = $path.$filename; //create file path

//check ipad_inventory.csv file
if (file_exists($csv_to_read) && ($handle = fopen($csv_to_read, "r")) !== FALSE)
{
	$i = 0;
  	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
		{
             if($i != 0){      
           		
				$item_number	= $data[0];
				$whs_id			= $data[1];
				$vendor			= $data[2];
				$port			= $data[3];
				$name			= $data[4];
				$demensions		= $data[5];
				$cube_price		= $data[6];
				$wh_price		= $data[7];
				$dir_price		= $data[8];
				$quantity		= $data[9];
				$ship_date		= $data[10];
				$ship_qty		= $data[11];
				
				 $find = array(' ', '"', ',');
				 $link_rewrite_string = str_replace($find, '-', $name);
				 $link_rewrite_small = strtolower($link_rewrite_string);
				$link_rewrite = str_replace('--', '-', $link_rewrite_small);
				 
				 
				$demensions = mysql_escape_string($demensions);
				$name = mysql_escape_string($name);
				
				
				
		 
				 Db::getInstance()->execute('
				INSERT INTO `'._DB_PREFIX_.'product` (`id_category_default`, `price`, `active`, `redirect_type`, `indexed`, `date_add`, `date_upd`,`item_number`, `ship_date`,`demensions`) VALUES (2, '.$wh_price.', 1, 404, 1, "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'", "'.$item_number.'", "'.$ship_date.'", "'.$demensions.'")'
				);
				

				$id_product = Db::getInstance()->Insert_ID(); 
				 Db::getInstance()->execute('
				INSERT INTO `'._DB_PREFIX_.'product_shop` (`id_product`, `id_shop`,`id_category_default`, `price`, `active`, `redirect_type`, `indexed`, `date_add`, `date_upd`) VALUES ('.$id_product.', 1, 2, '.$wh_price.', 1, 404, 1, "'.date('Y-m-d H:i:s').'", "'.date('Y-m-d H:i:s').'")'
				);
				
				 
				  Db::getInstance()->execute('
				INSERT INTO `'._DB_PREFIX_.'category_product` (`id_category`, `id_product`) VALUES (2, '.$id_product.')'
				);
				
				 Db::getInstance()->execute('
				INSERT INTO `'._DB_PREFIX_.'product_lang` (`id_product`, `id_lang`,name,link_rewrite) VALUES ('.$id_product.', 1, "'.$name.'", "'.$link_rewrite.'")'
				);
				
			
		    	Db::getInstance()->execute('
				INSERT INTO `'._DB_PREFIX_.'stock_available` (`id_product`, `id_product_attribute`,`id_shop` , `id_shop_group`,`quantity`, `depends_on_stock`) VALUES ('.$id_product.', 0, 1, 0, '.(int)$quantity.', 2)'
				);
				
				
				if($i>5){
				echo "done"; exit;
				}
				
				
			 }
			  
			 
			 $i++;     
        }  
	
}
echo "done:".$timerStart.":".(time()-$start_time).":";
exit;



?>