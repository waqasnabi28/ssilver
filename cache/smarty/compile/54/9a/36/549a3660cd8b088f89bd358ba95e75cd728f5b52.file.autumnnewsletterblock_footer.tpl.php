<?php /* Smarty version Smarty-3.1.14, created on 2013-10-25 07:12:35
         compiled from "D:\xampp\htdocs\ssilver\modules\autumnnewsletterblock\views\templates\hook\autumnnewsletterblock_footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21611526a52234291b2-38972852%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '549a3660cd8b088f89bd358ba95e75cd728f5b52' => 
    array (
      0 => 'D:\\xampp\\htdocs\\ssilver\\modules\\autumnnewsletterblock\\views\\templates\\hook\\autumnnewsletterblock_footer.tpl',
      1 => 1382697954,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21611526a52234291b2-38972852',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'value' => 0,
    'msg' => 0,
    'nw_error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526a52234b6fe2_07135795',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526a52234b6fe2_07135795')) {function content_526a52234b6fe2_07135795($_smarty_tpl) {?>

<!-- Block Newsletter module-->

<div id="newsletter_block_footer" class="block">
    <h4><?php echo smartyTranslate(array('s'=>'Subscribe To Our Newsletter','mod'=>'autumnnewsletterblock'),$_smarty_tpl);?>
</h4>
    <a href="#" class="open-close-footer"></a>
    <div class="block_content">

        <p class="newsletter_desc">
            <?php echo smartyTranslate(array('s'=>'Sign-up our newsletter to be the first to know about Sales, New Products and Exclusive Offers.','mod'=>'autumnnewsletterblock'),$_smarty_tpl);?>

        </p>

        <form action="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('index');?>
" method="post">
            <p>
                
                <input type="text" name="email" size="18" 
                       value="<?php if (isset($_smarty_tpl->tpl_vars['value']->value)&&$_smarty_tpl->tpl_vars['value']->value){?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php }else{ ?><?php echo smartyTranslate(array('s'=>'e-mail address','mod'=>'autumnnewsletterblock'),$_smarty_tpl);?>
<?php }?>" 
                       onfocus="javascript:if (this.value == '<?php echo smartyTranslate(array('s'=>'e-mail address','mod'=>'autumnnewsletterblock'),$_smarty_tpl);?>
')
                                   this.value = '';" 
                       onblur="javascript:if (this.value == '')
                                   this.value = '<?php echo smartyTranslate(array('s'=>'e-mail address','mod'=>'autumnnewsletterblock'),$_smarty_tpl);?>
';" 
                       class="newsletter_input dark" />
            </p>
            <p style="margin-top: 10px;">
                <select name="action" style="border: none;">
                    <option value="0"><?php echo smartyTranslate(array('s'=>'Subscribe','mod'=>'autumnnewsletterblock'),$_smarty_tpl);?>
</option>
                    <option value="1"><?php echo smartyTranslate(array('s'=>'Unsubscribe','mod'=>'autumnnewsletterblock'),$_smarty_tpl);?>
</option>
                </select>
                <input type="submit" value="ok" class="submit_orange" name="submitNewsletter" />
            </p>
        </form>

        <?php if (isset($_smarty_tpl->tpl_vars['msg']->value)&&$_smarty_tpl->tpl_vars['msg']->value){?>
            <p class="<?php if ($_smarty_tpl->tpl_vars['nw_error']->value){?>newsletter_warning_inline<?php }else{ ?>newsletter_success_inline<?php }?>"><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</p>
        <?php }?>
    </div>
</div>
<!-- /Block Newsletter module-->
<?php }} ?>