<?php /* Smarty version Smarty-3.1.14, created on 2013-10-25 07:12:32
         compiled from "D:\xampp\htdocs\ssilver\themes\autumn\modules\blocklanguages\blocklanguages.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1837526a5220c9d3b1-32315012%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '541f6a0bca134aa7b6543159255aab8358af6ae8' => 
    array (
      0 => 'D:\\xampp\\htdocs\\ssilver\\themes\\autumn\\modules\\blocklanguages\\blocklanguages.tpl',
      1 => 1382697940,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1837526a5220c9d3b1-32315012',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'languages' => 0,
    'language' => 0,
    'lang_iso' => 0,
    'indice_lang' => 0,
    'lang_rewrite_urls' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526a5220d85900_13232332',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526a5220d85900_13232332')) {function content_526a5220d85900_13232332($_smarty_tpl) {?>


<?php if (count($_smarty_tpl->tpl_vars['languages']->value)>1){?>

<script>
    $('document').ready(function(){
        $("#first-languages.selectbox").change(function() {
            window.location = $(this).find("option:selected").val();
        });
    });
</script>
<div id="languages_block_top" >
	<div id="countries">
	   <select id="first-languages" class="selectbox countries_select">
		<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['language']->key;
?>
			<option <?php if ($_smarty_tpl->tpl_vars['language']->value['iso_code']==$_smarty_tpl->tpl_vars['lang_iso']->value){?>selected="selected"<?php }?>

                            <?php if ($_smarty_tpl->tpl_vars['language']->value['iso_code']!=$_smarty_tpl->tpl_vars['lang_iso']->value){?>
                                <?php $_smarty_tpl->tpl_vars['indice_lang'] = new Smarty_variable($_smarty_tpl->tpl_vars['language']->value['id_lang'], null, 0);?>

                                <?php if (isset($_smarty_tpl->tpl_vars['lang_rewrite_urls']->value[$_smarty_tpl->tpl_vars['indice_lang']->value])){?>
                                        value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['lang_rewrite_urls']->value[$_smarty_tpl->tpl_vars['indice_lang']->value], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
">
                                <?php }else{ ?>
                                        value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getLanguageLink($_smarty_tpl->tpl_vars['language']->value['id_lang']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
">

                                <?php }?>
                            <?php }else{ ?>
                                >
                            <?php }?>
                                       <?php echo mb_strtoupper($_smarty_tpl->tpl_vars['language']->value['iso_code'], 'UTF-8');?>


			</option>
		<?php } ?>
            </select>
	</div>
</div>
<?php }?>
<?php }} ?>