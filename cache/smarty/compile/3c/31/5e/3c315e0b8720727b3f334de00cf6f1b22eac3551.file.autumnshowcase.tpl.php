<?php /* Smarty version Smarty-3.1.14, created on 2013-10-25 07:12:34
         compiled from "D:\xampp\htdocs\ssilver\modules\autumnshowcase\autumnshowcase.tpl" */ ?>
<?php /*%%SmartyHeaderCode:25167526a5222584c83-38176856%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3c315e0b8720727b3f334de00cf6f1b22eac3551' => 
    array (
      0 => 'D:\\xampp\\htdocs\\ssilver\\modules\\autumnshowcase\\autumnshowcase.tpl',
      1 => 1382697956,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '25167526a5222584c83-38176856',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'showcases' => 0,
    'image_shape' => 0,
    'featured_products' => 0,
    'featured' => 0,
    'link' => 0,
    'featured_prices' => 0,
    'restricted_country_mode' => 0,
    'PS_CATALOG_MODE' => 0,
    'priceDisplay' => 0,
    'featured_cart' => 0,
    'add_prod_display' => 0,
    'static_token' => 0,
    'new_products' => 0,
    'new' => 0,
    'new_prices' => 0,
    'new_cart' => 0,
    'special_products' => 0,
    'special' => 0,
    'special_prices' => 0,
    'special_cart' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526a5222e054d7_22205077',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526a5222e054d7_22205077')) {function content_526a5222e054d7_22205077($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'D:\\xampp\\htdocs\\ssilver\\tools\\smarty\\plugins\\modifier.escape.php';
?>
<script type="text/javascript">  
    var autoscroll = <?php if (isset($_smarty_tpl->tpl_vars['showcases']->value['autoscroll'])&&$_smarty_tpl->tpl_vars['showcases']->value['autoscroll']){?>true<?php }else{ ?>false<?php }?>;
    var autoscrollInterval = <?php if (isset($_smarty_tpl->tpl_vars['showcases']->value['autoscrollInterval'])&&$_smarty_tpl->tpl_vars['showcases']->value['autoscrollInterval']){?><?php echo $_smarty_tpl->tpl_vars['showcases']->value['autoscrollInterval'];?>
<?php }else{ ?>0<?php }?>;
</script>

    <?php if (isset($_smarty_tpl->tpl_vars['showcases']->value['featured'])&&$_smarty_tpl->tpl_vars['showcases']->value['featured']){?>
        
        <div class="autumnshowcase_block group featured_products <?php echo isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_img' : ' ';?>
">
            
            <div class="title_block group">
                
                <h4><?php echo smartyTranslate(array('s'=>'Featured products','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</h4>

                <div class="carousel_controls <?php if (count($_smarty_tpl->tpl_vars['featured_products']->value)<=5){?>hide<?php }?>">
                    <a class="carousel_prev" href="#"></a>
                    <a class="carousel_next" href="#"></a>
                </div>

            </div>

	<?php if (isset($_smarty_tpl->tpl_vars['featured_products']->value)&&$_smarty_tpl->tpl_vars['featured_products']->value){?>
            <div class="autumnshowcase_carousel">
                <ul>
                    
                <?php  $_smarty_tpl->tpl_vars['featured'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['featured']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['featured_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['featured']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['featured']->iteration=0;
 $_smarty_tpl->tpl_vars['featured']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['featured']->key => $_smarty_tpl->tpl_vars['featured']->value){
$_smarty_tpl->tpl_vars['featured']->_loop = true;
 $_smarty_tpl->tpl_vars['featured']->iteration++;
 $_smarty_tpl->tpl_vars['featured']->index++;
 $_smarty_tpl->tpl_vars['featured']->first = $_smarty_tpl->tpl_vars['featured']->index === 0;
 $_smarty_tpl->tpl_vars['featured']->last = $_smarty_tpl->tpl_vars['featured']->iteration === $_smarty_tpl->tpl_vars['featured']->total;
?>
                    <li class="ajax_block_product <?php if ($_smarty_tpl->tpl_vars['featured']->first){?>first_item<?php }elseif($_smarty_tpl->tpl_vars['featured']->last){?>last_item<?php }else{ ?>item<?php }?>">
                        
                        <a class="image_link" href="<?php echo $_smarty_tpl->tpl_vars['featured']->value['link'];?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['featured']->value['name'], 'html', 'UTF-8');?>
">
                            <?php if (isset($_smarty_tpl->tpl_vars['featured']->value['new'])&&$_smarty_tpl->tpl_vars['featured']->value['new']==1){?><span class="new"><?php echo smartyTranslate(array('s'=>'New','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span><?php }?>
                            <img class="product_image" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['featured']->value['link_rewrite'],$_smarty_tpl->tpl_vars['featured']->value['id_image'],isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_default' : 'home_default');?>
" alt="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['featured']->value['name'], 'html', 'UTF-8');?>
" />
                        </a>

                        <div class="product_details">
                            <h5><a href="<?php echo $_smarty_tpl->tpl_vars['featured']->value['link'];?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['featured']->value['name'], 'htmlall', 'UTF-8');?>
"><?php echo smarty_modifier_escape($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['featured']->value['name'],28,'...'), 'htmlall', 'UTF-8');?>
</a></h5>

                            <?php if ($_smarty_tpl->tpl_vars['featured_prices']->value&&$_smarty_tpl->tpl_vars['featured']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                <div class="price_container">
                                    <span class="price"><?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value){?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['featured']->value['price']),$_smarty_tpl);?>
<?php }else{ ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['featured']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?></span>

                                    <?php if (isset($_smarty_tpl->tpl_vars['featured']->value['reduction'])&&$_smarty_tpl->tpl_vars['featured']->value['reduction']!=0){?>
                                        <span class="old-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['featured']->value['price_without_reduction']),$_smarty_tpl);?>
</span>
                                    <?php }?>

                                    <?php if (isset($_smarty_tpl->tpl_vars['featured']->value['on_sale'])&&$_smarty_tpl->tpl_vars['featured']->value['on_sale']&&isset($_smarty_tpl->tpl_vars['featured']->value['show_price'])&&$_smarty_tpl->tpl_vars['featured']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                      <span class="discount"><?php echo smartyTranslate(array('s'=>'On sale!','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span>     
                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['featured']->value['reduction'])&&$_smarty_tpl->tpl_vars['featured']->value['reduction']&&isset($_smarty_tpl->tpl_vars['featured']->value['show_price'])&&$_smarty_tpl->tpl_vars['featured']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                     <span class="discount"><?php echo smartyTranslate(array('s'=>'Discount!','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span>
                                    <?php }?>

                                </div>
                            <?php }?>
			</div>
                        
                        <?php if ($_smarty_tpl->tpl_vars['featured_cart']->value){?>
                            <?php if (($_smarty_tpl->tpl_vars['featured']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['featured']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['featured']->value['minimal_quantity']==1&&$_smarty_tpl->tpl_vars['featured']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                
                                <?php if (($_smarty_tpl->tpl_vars['featured']->value['quantity']>0||$_smarty_tpl->tpl_vars['featured']->value['allow_oosp'])){?>
                                    <a class="ajax_add_to_cart_button autumn-button" rel="ajax_id_product_<?php echo $_smarty_tpl->tpl_vars['featured']->value['id_product'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart');?>
?qty=1&amp;id_product=<?php echo $_smarty_tpl->tpl_vars['featured']->value['id_product'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
&amp;add" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'autumnshowcase'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</a>
                                <?php }else{ ?>
                                    
                                <?php }?>
                                
                            <?php }?>
                        <?php }?>
                        
                    </li>
                <?php } ?>
                
                </ul>
            </div>
	<?php }else{ ?>
            
            <p><?php echo smartyTranslate(array('s'=>'No featured products','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</p>
            
	<?php }?>
        
        </div>
        
    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['showcases']->value['new'])&&$_smarty_tpl->tpl_vars['showcases']->value['new']){?>
              
        <div class="autumnshowcase_block group new_products <?php echo isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_img' : ' ';?>
">
            
            <div class="title_block group">
                
                <h4><?php echo smartyTranslate(array('s'=>'New products','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</h4>

                <div class="carousel_controls <?php if (count($_smarty_tpl->tpl_vars['new_products']->value)<=5){?>hide<?php }?>">
                    <a class="carousel_prev" href="#"></a>
                    <a class="carousel_next" href="#"></a>
                </div>

            </div>
        
	<?php if (isset($_smarty_tpl->tpl_vars['new_products']->value)&&$_smarty_tpl->tpl_vars['new_products']->value){?>
            <div class="autumnshowcase_carousel">
                <ul>
                    
                <?php  $_smarty_tpl->tpl_vars['new'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['new']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['new_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['new']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['new']->iteration=0;
 $_smarty_tpl->tpl_vars['new']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['new']->key => $_smarty_tpl->tpl_vars['new']->value){
$_smarty_tpl->tpl_vars['new']->_loop = true;
 $_smarty_tpl->tpl_vars['new']->iteration++;
 $_smarty_tpl->tpl_vars['new']->index++;
 $_smarty_tpl->tpl_vars['new']->first = $_smarty_tpl->tpl_vars['new']->index === 0;
 $_smarty_tpl->tpl_vars['new']->last = $_smarty_tpl->tpl_vars['new']->iteration === $_smarty_tpl->tpl_vars['new']->total;
?>
                    <li class="ajax_block_product <?php if ($_smarty_tpl->tpl_vars['new']->first){?>first_item<?php }elseif($_smarty_tpl->tpl_vars['new']->last){?>last_item<?php }else{ ?>item<?php }?>">
                                                
                        <a class="image_link" href="<?php echo $_smarty_tpl->tpl_vars['new']->value['link'];?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['new']->value['name'], 'html', 'UTF-8');?>
">
                            <?php if (isset($_smarty_tpl->tpl_vars['new']->value['new'])&&$_smarty_tpl->tpl_vars['new']->value['new']==1){?><span class="new"><?php echo smartyTranslate(array('s'=>'New','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span><?php }?>
                            <img class="product_image" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['new']->value['link_rewrite'],$_smarty_tpl->tpl_vars['new']->value['id_image'],isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_default' : 'home_default');?>
" alt="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['new']->value['name'], 'html', 'UTF-8');?>
" />
                        </a>
                        
                        <div class="product_details">
                            <h5><a href="<?php echo $_smarty_tpl->tpl_vars['new']->value['link'];?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['new']->value['name'], 'htmlall', 'UTF-8');?>
"><?php echo smarty_modifier_escape($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['new']->value['name'],28,'...'), 'htmlall', 'UTF-8');?>
</a></h5>

                            <?php if ($_smarty_tpl->tpl_vars['new_prices']->value&&$_smarty_tpl->tpl_vars['new']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                <div class="price_container">
                                    <span class="price"><?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value){?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['new']->value['price']),$_smarty_tpl);?>
<?php }else{ ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['new']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?></span>

                                    <?php if (isset($_smarty_tpl->tpl_vars['new']->value['reduction'])&&$_smarty_tpl->tpl_vars['new']->value['reduction']!=0){?>
                                        <span class="old-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['new']->value['price_without_reduction']),$_smarty_tpl);?>
</span>
                                    <?php }?>

                                    <?php if (isset($_smarty_tpl->tpl_vars['new']->value['on_sale'])&&$_smarty_tpl->tpl_vars['new']->value['on_sale']&&isset($_smarty_tpl->tpl_vars['new']->value['show_price'])&&$_smarty_tpl->tpl_vars['new']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                     <span class="discount"><?php echo smartyTranslate(array('s'=>'On sale!','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span>     
                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['new']->value['reduction'])&&$_smarty_tpl->tpl_vars['new']->value['reduction']&&isset($_smarty_tpl->tpl_vars['new']->value['show_price'])&&$_smarty_tpl->tpl_vars['new']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                     <span class="discount"><?php echo smartyTranslate(array('s'=>'Discount!','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span>
                                    <?php }?>

                                </div>
                            <?php }?>
			</div>
                        
                        <?php if ($_smarty_tpl->tpl_vars['new_cart']->value){?>
                            <?php if (($_smarty_tpl->tpl_vars['new']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['new']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['new']->value['minimal_quantity']==1&&$_smarty_tpl->tpl_vars['new']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                <?php if (($_smarty_tpl->tpl_vars['new']->value['quantity']>0||$_smarty_tpl->tpl_vars['new']->value['allow_oosp'])){?>
                                    <a class="ajax_add_to_cart_button autumn-button" rel="ajax_id_product_<?php echo $_smarty_tpl->tpl_vars['new']->value['id_product'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart');?>
?qty=1&amp;id_product=<?php echo $_smarty_tpl->tpl_vars['new']->value['id_product'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
&amp;add" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'autumnshowcase'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</a>
                                <?php }else{ ?>
                                   
                                <?php }?>
                            <?php }?>
                        <?php }?>
                        
                    </li>
                <?php } ?>
                
                </ul>
            </div>
	<?php }else{ ?>
            
            <p><?php echo smartyTranslate(array('s'=>'No new products','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</p>
            
	<?php }?>
        
        </div>
        
    <?php }?>

    
    
    
    <?php if (isset($_smarty_tpl->tpl_vars['showcases']->value['special'])&&$_smarty_tpl->tpl_vars['showcases']->value['special']){?>
               
        <div class="autumnshowcase_block group special_products <?php echo isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_img' : ' ';?>
">
            
            <div class="title_block group">
                
                <h4><?php echo smartyTranslate(array('s'=>'Special products','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</h4>

                <div class="carousel_controls <?php if (count($_smarty_tpl->tpl_vars['special_products']->value)<=5){?>hide<?php }?>">
                    <a class="carousel_prev" href="#"></a>
                    <a class="carousel_next" href="#"></a>
                </div>

            </div>
        
	<?php if (isset($_smarty_tpl->tpl_vars['special_products']->value)&&$_smarty_tpl->tpl_vars['special_products']->value){?>
            <div class="autumnshowcase_carousel">
                <ul>
                    
                <?php  $_smarty_tpl->tpl_vars['special'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['special']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['special_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['special']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['special']->iteration=0;
 $_smarty_tpl->tpl_vars['special']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['special']->key => $_smarty_tpl->tpl_vars['special']->value){
$_smarty_tpl->tpl_vars['special']->_loop = true;
 $_smarty_tpl->tpl_vars['special']->iteration++;
 $_smarty_tpl->tpl_vars['special']->index++;
 $_smarty_tpl->tpl_vars['special']->first = $_smarty_tpl->tpl_vars['special']->index === 0;
 $_smarty_tpl->tpl_vars['special']->last = $_smarty_tpl->tpl_vars['special']->iteration === $_smarty_tpl->tpl_vars['special']->total;
?>
                    <li class="ajax_block_product <?php if ($_smarty_tpl->tpl_vars['special']->first){?>first_item<?php }elseif($_smarty_tpl->tpl_vars['special']->last){?>last_item<?php }else{ ?>item<?php }?>">
                                                
                        <a class="image_link" href="<?php echo $_smarty_tpl->tpl_vars['special']->value['link'];?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['special']->value['name'], 'html', 'UTF-8');?>
">
                            <?php if (isset($_smarty_tpl->tpl_vars['special']->value['new'])&&$_smarty_tpl->tpl_vars['special']->value['new']==1){?><span class="new"><?php echo smartyTranslate(array('s'=>'New','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span><?php }?>
                            <img class="product_image" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['special']->value['link_rewrite'],$_smarty_tpl->tpl_vars['special']->value['id_image'],isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_default' : 'home_default');?>
" alt="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['special']->value['name'], 'html', 'UTF-8');?>
" />
                        </a>
                        
                        <div class="product_details">
                            <h5><a href="<?php echo $_smarty_tpl->tpl_vars['special']->value['link'];?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['special']->value['name'], 'htmlall', 'UTF-8');?>
"><?php echo smarty_modifier_escape($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['special']->value['name'],28,'...'), 'htmlall', 'UTF-8');?>
</a></h5>

                            <?php if ($_smarty_tpl->tpl_vars['special_prices']->value&&$_smarty_tpl->tpl_vars['special']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                <div class="price_container">
                                    <span class="price"><?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value){?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['special']->value['price']),$_smarty_tpl);?>
<?php }else{ ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['special']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?></span>

                                    <?php if (isset($_smarty_tpl->tpl_vars['special']->value['reduction'])&&$_smarty_tpl->tpl_vars['special']->value['reduction']!=0){?>
                                        <span class="old-price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['special']->value['price_without_reduction']),$_smarty_tpl);?>
</span>
                                    <?php }?>

                                    <?php if (isset($_smarty_tpl->tpl_vars['special']->value['on_sale'])&&$_smarty_tpl->tpl_vars['special']->value['on_sale']&&isset($_smarty_tpl->tpl_vars['special']->value['show_price'])&&$_smarty_tpl->tpl_vars['special']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                     <span class="discount"><?php echo smartyTranslate(array('s'=>'On sale!','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span>     
                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['special']->value['reduction'])&&$_smarty_tpl->tpl_vars['special']->value['reduction']&&isset($_smarty_tpl->tpl_vars['special']->value['show_price'])&&$_smarty_tpl->tpl_vars['special']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                     <span class="discount"><?php echo smartyTranslate(array('s'=>'Discount!','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</span>
                                    <?php }?>

                                </div>
                            <?php }?>
			</div>
                        
                        <?php if ($_smarty_tpl->tpl_vars['special_cart']->value){?>
                            <?php if (($_smarty_tpl->tpl_vars['special']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['special']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['special']->value['minimal_quantity']==1&&$_smarty_tpl->tpl_vars['special']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
                                <?php if (($_smarty_tpl->tpl_vars['special']->value['quantity']>0||$_smarty_tpl->tpl_vars['special']->value['allow_oosp'])){?>
                                    <a class="ajax_add_to_cart_button autumn-button" rel="ajax_id_product_<?php echo $_smarty_tpl->tpl_vars['special']->value['id_product'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getPageLink('cart');?>
?qty=1&amp;id_product=<?php echo $_smarty_tpl->tpl_vars['special']->value['id_product'];?>
&amp;token=<?php echo $_smarty_tpl->tpl_vars['static_token']->value;?>
&amp;add" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'autumnshowcase'),$_smarty_tpl);?>
"><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</a>
                                <?php }else{ ?>
                                    
                                <?php }?>
                            <?php }?>
                        <?php }?>
                        
                    </li>
                <?php } ?>
                
                </ul>
            </div>
	<?php }else{ ?>
            
            <p><?php echo smartyTranslate(array('s'=>'No special products','mod'=>'autumnshowcase'),$_smarty_tpl);?>
</p>
            
	<?php }?>
        
        </div>
    <?php }?><?php }} ?>