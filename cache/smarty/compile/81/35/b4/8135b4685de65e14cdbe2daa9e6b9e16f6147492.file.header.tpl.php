<?php /* Smarty version Smarty-3.1.14, created on 2013-10-25 07:12:35
         compiled from "D:\xampp\htdocs\ssilver\themes\autumn\header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:27526526a5223569087-50103506%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8135b4685de65e14cdbe2daa9e6b9e16f6147492' => 
    array (
      0 => 'D:\\xampp\\htdocs\\ssilver\\themes\\autumn\\header.tpl',
      1 => 1382697938,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '27526526a5223569087-50103506',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang_iso' => 0,
    'meta_title' => 0,
    'meta_description' => 0,
    'meta_keywords' => 0,
    'nobots' => 0,
    'nofollow' => 0,
    'favicon_url' => 0,
    'img_update_time' => 0,
    'page_name' => 0,
    'have_image' => 0,
    'product' => 0,
    'cover' => 0,
    'link' => 0,
    'content_dir' => 0,
    'base_uri' => 0,
    'static_token' => 0,
    'token' => 0,
    'priceDisplayPrecision' => 0,
    'currency' => 0,
    'priceDisplay' => 0,
    'roundMode' => 0,
    'css_files' => 0,
    'css_uri' => 0,
    'media' => 0,
    'css_dir' => 0,
    'js_files' => 0,
    'js_uri' => 0,
    'HOOK_HEADER' => 0,
    'color_changer' => 0,
    'logo_width' => 0,
    'hide_left_column' => 0,
    'hide_right_column' => 0,
    'content_only' => 0,
    'restricted_country_mode' => 0,
    'geolocation_country' => 0,
    'base_dir' => 0,
    'shop_name' => 0,
    'logo_url' => 0,
    'HOOK_TOP' => 0,
    'slide_config' => 0,
    'slides' => 0,
    'slide' => 0,
    'HOOK_LEFT_COLUMN' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526a522387e7c9_96651356',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526a522387e7c9_96651356')) {function content_526a522387e7c9_96651356($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'D:\\xampp\\htdocs\\ssilver\\tools\\smarty\\plugins\\modifier.escape.php';
?>
<!DOCTYPE html>
<html lang="<?php echo $_smarty_tpl->tpl_vars['lang_iso']->value;?>
">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_title']->value, 'vhtmlall', 'UTF-8');?>
</title>
    <?php if (isset($_smarty_tpl->tpl_vars['meta_description']->value)&&$_smarty_tpl->tpl_vars['meta_description']->value){?>
        <meta name="description" content="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_description']->value, 'html', 'UTF-8');?>
" />
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['meta_keywords']->value)&&$_smarty_tpl->tpl_vars['meta_keywords']->value){?>
        <meta name="keywords" content="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_keywords']->value, 'html', 'UTF-8');?>
" />
    <?php }?>
    <meta name="robots" content="<?php if (isset($_smarty_tpl->tpl_vars['nobots']->value)){?>no<?php }?>index,<?php if (isset($_smarty_tpl->tpl_vars['nofollow']->value)&&$_smarty_tpl->tpl_vars['nofollow']->value){?>no<?php }?>follow" />
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo $_smarty_tpl->tpl_vars['favicon_url']->value;?>
?<?php echo $_smarty_tpl->tpl_vars['img_update_time']->value;?>
" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $_smarty_tpl->tpl_vars['favicon_url']->value;?>
?<?php echo $_smarty_tpl->tpl_vars['img_update_time']->value;?>
" />

    
    <meta property="og:title" content="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_title']->value, 'vhtmlall', 'UTF-8');?>
" />
    <?php if (isset($_smarty_tpl->tpl_vars['meta_description']->value)&&$_smarty_tpl->tpl_vars['meta_description']->value){?><meta property="og:description" content="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['meta_description']->value, 'html', 'UTF-8');?>
" /><?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['page_name']->value)&&$_smarty_tpl->tpl_vars['page_name']->value=="product"&&isset($_smarty_tpl->tpl_vars['have_image']->value)&&$_smarty_tpl->tpl_vars['have_image']->value){?><meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite,$_smarty_tpl->tpl_vars['cover']->value['id_image'],'large_default');?>
" /><?php }?>


    <script type="text/javascript">
        var baseDir = '<?php echo addslashes($_smarty_tpl->tpl_vars['content_dir']->value);?>
';
        var baseUri = '<?php echo addslashes($_smarty_tpl->tpl_vars['base_uri']->value);?>
';
        var static_token = '<?php echo addslashes($_smarty_tpl->tpl_vars['static_token']->value);?>
';
        var token = '<?php echo addslashes($_smarty_tpl->tpl_vars['token']->value);?>
';
        var priceDisplayPrecision = <?php echo $_smarty_tpl->tpl_vars['priceDisplayPrecision']->value*$_smarty_tpl->tpl_vars['currency']->value->decimals;?>
;
        var priceDisplayMethod = <?php echo $_smarty_tpl->tpl_vars['priceDisplay']->value;?>
;
        var roundMode = <?php echo $_smarty_tpl->tpl_vars['roundMode']->value;?>
;
    </script>
    <?php if (isset($_smarty_tpl->tpl_vars['css_files']->value)){?>
        <?php  $_smarty_tpl->tpl_vars['media'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['media']->_loop = false;
 $_smarty_tpl->tpl_vars['css_uri'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['css_files']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['media']->key => $_smarty_tpl->tpl_vars['media']->value){
$_smarty_tpl->tpl_vars['media']->_loop = true;
 $_smarty_tpl->tpl_vars['css_uri']->value = $_smarty_tpl->tpl_vars['media']->key;
?>
            <link href="<?php echo $_smarty_tpl->tpl_vars['css_uri']->value;?>
" rel="stylesheet" type="text/css" media="<?php echo $_smarty_tpl->tpl_vars['media']->value;?>
" />
        <?php } ?>
    <?php }?>

    <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['css_dir']->value;?>
ie8.css" />
    <![endif]-->

    <?php if (isset($_smarty_tpl->tpl_vars['js_files']->value)){?>
        <?php  $_smarty_tpl->tpl_vars['js_uri'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['js_uri']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['js_files']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['js_uri']->key => $_smarty_tpl->tpl_vars['js_uri']->value){
$_smarty_tpl->tpl_vars['js_uri']->_loop = true;
?>
            <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_uri']->value;?>
"></script>
        <?php } ?>
    <?php }?>
    <?php echo $_smarty_tpl->tpl_vars['HOOK_HEADER']->value;?>


    
    <?php echo $_smarty_tpl->tpl_vars['color_changer']->value;?>

    <?php echo $_smarty_tpl->tpl_vars['logo_width']->value;?>

</head>
<body itemscope itemtype="http://schema.org/WebPage" <?php if (isset($_smarty_tpl->tpl_vars['page_name']->value)){?>id="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['page_name']->value, 'htmlall', 'UTF-8');?>
"<?php }?> class="<?php if ($_smarty_tpl->tpl_vars['hide_left_column']->value){?>hide-left-column<?php }?> <?php if ($_smarty_tpl->tpl_vars['hide_right_column']->value){?>hide-right-column<?php }?> <?php if ($_smarty_tpl->tpl_vars['content_only']->value){?> content_only <?php }?>">
<?php if (!$_smarty_tpl->tpl_vars['content_only']->value){?>
<?php if (isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['restricted_country_mode']->value){?>
    <div id="restricted-country">
        <p><?php echo smartyTranslate(array('s'=>'You cannot place a new order from your country.'),$_smarty_tpl);?>
 <span class="bold"><?php echo $_smarty_tpl->tpl_vars['geolocation_country']->value;?>
</span></p>
    </div>
<?php }?>
<div id="wrapper" class="group">

    <div id="header-fluid" class="fluid-bg">
        <div class="container">
            <!-- Header -->
            <div id="header" class="column full">
                <div id="header-logo">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['shop_name']->value, 'htmlall', 'UTF-8');?>
">
                        <img class="logo" src="<?php echo $_smarty_tpl->tpl_vars['logo_url']->value;?>
" alt="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['shop_name']->value, 'htmlall', 'UTF-8');?>
" />
                    </a>
                </div>

                <div id="header-right">
                    <?php echo $_smarty_tpl->tpl_vars['HOOK_TOP']->value;?>

                </div>
            </div>
        </div>
    </div>

    <div id="content-fluid" class="fluid-bg">

        <?php if ($_smarty_tpl->tpl_vars['page_name']->value=="index"){?>

            <?php if (isset($_smarty_tpl->tpl_vars['slide_config']->value)){?> 
                <script type="text/javascript">
                    $(window).load(function() {
                        var s_effect = <?php echo $_smarty_tpl->tpl_vars['slide_config']->value['effect'];?>
;
                        var s_animspeed = <?php echo $_smarty_tpl->tpl_vars['slide_config']->value['anim_speed'];?>
;
                        var s_pausetime = <?php echo $_smarty_tpl->tpl_vars['slide_config']->value['pause_time'];?>
;

                        $('#autumn-slider').nivoSlider({
                            effect: s_effect,
                            pauseTime: s_pausetime,
                            animSpeed: s_animspeed,
                            controlNav: false
                        });

                        $('#autumn-slider').touchwipe({
                            wipeLeft: function() {
                                $('a.nivo-nextNav').trigger('click');
                            },

                            wipeRight: function() {
                                $('a.nivo-prevNav').trigger('click');
                            },

                            preventDefaultEvents: false
                        });

                    });
                </script>
            <?php }?>

            <?php if (isset($_smarty_tpl->tpl_vars['slides']->value)&&isset($_smarty_tpl->tpl_vars['page_name']->value)&&$_smarty_tpl->tpl_vars['page_name']->value=="index"){?>
                <div id="autumn-slider" class="nivoSlider">
                    <?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value){
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
                        <?php if ($_smarty_tpl->tpl_vars['slide']->value['is_active']==1){?>
                            
                            <?php if ($_smarty_tpl->tpl_vars['slide']->value['url']!=''){?><a class="nivo-imageLink" href="<?php echo $_smarty_tpl->tpl_vars['slide']->value['url'];?>
"><?php }?>
                            <img src="<?php echo @constant('_MODULE_DIR_');?>
/autumnslider/slides/<?php echo $_smarty_tpl->tpl_vars['slide']->value['image'];?>
" />
                            <?php if ($_smarty_tpl->tpl_vars['slide']->value['url']!=''){?></a><?php }?>
                            
                        <?php }?>
                    <?php } ?>
                </div>
            <?php }?>

        <?php }?>

        <div class="content container">

            <div class="column <?php if ($_smarty_tpl->tpl_vars['HOOK_LEFT_COLUMN']->value){?>two-third<?php }else{ ?>full<?php }?>"><!-- Column -->
<?php }?><?php }} ?>