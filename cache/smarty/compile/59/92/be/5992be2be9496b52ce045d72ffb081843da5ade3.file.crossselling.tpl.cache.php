<?php /* Smarty version Smarty-3.1.14, created on 2013-10-25 07:29:03
         compiled from "D:\xampp\htdocs\ssilver\themes\autumn\modules\crossselling\crossselling.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6349526a55ffeeb070-53998710%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5992be2be9496b52ce045d72ffb081843da5ade3' => 
    array (
      0 => 'D:\\xampp\\htdocs\\ssilver\\themes\\autumn\\modules\\crossselling\\crossselling.tpl',
      1 => 1382697940,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6349526a55ffeeb070-53998710',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'orderProducts' => 0,
    'image_shape' => 0,
    'middlePosition_crossselling' => 0,
    'orderProduct' => 0,
    'link' => 0,
    'crossDisplayPrice' => 0,
    'restricted_country_mode' => 0,
    'PS_CATALOG_MODE' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526a56000f6107_49794228',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526a56000f6107_49794228')) {function content_526a56000f6107_49794228($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'D:\\xampp\\htdocs\\ssilver\\tools\\smarty\\plugins\\modifier.escape.php';
?>
<?php if (isset($_smarty_tpl->tpl_vars['orderProducts']->value)&&count($_smarty_tpl->tpl_vars['orderProducts']->value)){?>
<div id="blockcrossselling" class="clearfix <?php echo $_smarty_tpl->tpl_vars['image_shape']->value;?>
">
	<script type="text/javascript">var cs_middle = <?php echo $_smarty_tpl->tpl_vars['middlePosition_crossselling']->value;?>
;</script>
	        
        <div class="title_block group">

            <h4 class="productscategory_h4"><?php echo smartyTranslate(array('s'=>'Customers who bought this product also bought:','mod'=>'crossselling'),$_smarty_tpl);?>
</h4>

            <div class="carousel_controls <?php if (count($_smarty_tpl->tpl_vars['orderProducts']->value)<=5){?>hide<?php }?>">
                <a class="carousel_prev" href="#"></a>
                <a class="carousel_next" href="#"></a>
            </div>

        </div>
        
	<div id="crossselling">
        
		
		<div id="crossselling_list">
			<ul class="carousel_products">
				<?php  $_smarty_tpl->tpl_vars['orderProduct'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['orderProduct']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orderProducts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['orderProduct']->key => $_smarty_tpl->tpl_vars['orderProduct']->value){
$_smarty_tpl->tpl_vars['orderProduct']->_loop = true;
?>
				<li>
					<a href="<?php echo $_smarty_tpl->tpl_vars['orderProduct']->value['link'];?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['orderProduct']->value['name']);?>
" class="lnk_img">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['orderProduct']->value['link_rewrite'],(($_smarty_tpl->tpl_vars['orderProduct']->value['product_id']).('-')).($_smarty_tpl->tpl_vars['orderProduct']->value['id_image']),isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_default' : 'home_default');?>
" alt="<?php echo $_smarty_tpl->tpl_vars['orderProduct']->value['name'];?>
" />
                                        </a>
					<p class="product_name flex-caption"><a href="<?php echo $_smarty_tpl->tpl_vars['orderProduct']->value['link'];?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['orderProduct']->value['name']);?>
"><?php echo smarty_modifier_escape($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['orderProduct']->value['name'],25,'...'), 'htmlall', 'UTF-8');?>
</a></p>
					<?php if ($_smarty_tpl->tpl_vars['crossDisplayPrice']->value&&$_smarty_tpl->tpl_vars['orderProduct']->value['show_price']==1&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value){?>
						<span class="price_display">
							<span class="price"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['orderProduct']->value['displayed_price']),$_smarty_tpl);?>
</span>
						</span><br />
					<?php }else{ ?>
						<br />
					<?php }?>
					<!-- <a title="<?php echo smartyTranslate(array('s'=>'View','mod'=>'crossselling'),$_smarty_tpl);?>
" href="<?php echo $_smarty_tpl->tpl_vars['orderProduct']->value['link'];?>
" class="button_small"><?php echo smartyTranslate(array('s'=>'View','mod'=>'crossselling'),$_smarty_tpl);?>
</a><br /> -->
				</li>
				<?php } ?>
			</ul>
		</div>
	
	
        </div>
</div>
        
<script>
    $(document).ready(function(){
    
            $('#crossselling_list').jcarousel({
                'wrap': 'circular'
            });

            $('#blockcrossselling .carousel_prev').jcarouselControl({
                target: '-=1'
            });

            $('#blockcrossselling .carousel_next').jcarouselControl({
                target: '+=1'
            });

    });
</script>
        
        
<?php }?>
<?php }} ?>