<?php /* Smarty version Smarty-3.1.14, created on 2013-10-25 07:12:33
         compiled from "D:\xampp\htdocs\ssilver\modules\autumnmegamenu\autumnmegamenu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:29870526a5221b4dbc2-47726330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f297fa63f900fb99c10f576ca1922fd492aa3b1c' => 
    array (
      0 => 'D:\\xampp\\htdocs\\ssilver\\modules\\autumnmegamenu\\autumnmegamenu.tpl',
      1 => 1382697955,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29870526a5221b4dbc2-47726330',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'mega_menu' => 0,
    'positions' => 0,
    'root' => 0,
    'manufacturer' => 0,
    'link' => 0,
    'supplier' => 0,
    'category' => 0,
    'childName' => 0,
    'childStep2Link' => 0,
    'childStep2Name' => 0,
    'product' => 0,
    'image_shape' => 0,
    'cmsl_link' => 0,
    'cmsl_name' => 0,
    'cmsp_content' => 0,
    'responsive_menu' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526a522224cfb5_31659897',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526a522224cfb5_31659897')) {function content_526a522224cfb5_31659897($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'D:\\xampp\\htdocs\\ssilver\\tools\\smarty\\plugins\\modifier.escape.php';
?><ul id="megamenu">
<?php  $_smarty_tpl->tpl_vars['positions'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['positions']->_loop = false;
 $_smarty_tpl->tpl_vars['root'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['mega_menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['positions']->key => $_smarty_tpl->tpl_vars['positions']->value){
$_smarty_tpl->tpl_vars['positions']->_loop = true;
 $_smarty_tpl->tpl_vars['root']->value = $_smarty_tpl->tpl_vars['positions']->key;
?>
   
        <li class="root_menu">
            <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['link'])&&$_smarty_tpl->tpl_vars['positions']->value['link']!=''){?>
                <a class="root_link" href="<?php echo $_smarty_tpl->tpl_vars['positions']->value['link'];?>
">
                    <?php echo $_smarty_tpl->tpl_vars['root']->value;?>

                </a>
            <?php }else{ ?>
                <span class="root_link">
                    <?php echo $_smarty_tpl->tpl_vars['root']->value;?>

                </span>
            <?php }?>
                        
            <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['top'])||isset($_smarty_tpl->tpl_vars['positions']->value['bottom'])||isset($_smarty_tpl->tpl_vars['positions']->value['m_left'])||isset($_smarty_tpl->tpl_vars['positions']->value['m_right'])){?>
               
                <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['top']['top_hide'])&&isset($_smarty_tpl->tpl_vars['positions']->value['bottom']['bottom_hide'])&&isset($_smarty_tpl->tpl_vars['positions']->value['m_left']['HIDE'])&&isset($_smarty_tpl->tpl_vars['positions']->value['m_right']['HIDE'])){?>
                    
                <?php }else{ ?>
                
                    <div class="megamenu_context">                       
                        
                        <div class="left_col <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['m_right'])&&!isset($_smarty_tpl->tpl_vars['positions']->value['m_right']['HIDE'])){?>with_right<?php }?>">
                            <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['top'])&&!isset($_smarty_tpl->tpl_vars['positions']->value['top']['top_hide'])){?>
                                <div id="megamenu_top">
                                    <ul>
                                        <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['top']['top_manufacturers'])){?>
                                            <span class="title"><?php echo smartyTranslate(array('s'=>'Manufacturers','mod'=>'autumnmegamenu'),$_smarty_tpl);?>
</span>
                                            <?php  $_smarty_tpl->tpl_vars['manufacturer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['manufacturer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['top']['top_manufacturers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['manufacturer']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['manufacturer']->iteration=0;
 $_smarty_tpl->tpl_vars['manufacturer']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['manufacturer']->key => $_smarty_tpl->tpl_vars['manufacturer']->value){
$_smarty_tpl->tpl_vars['manufacturer']->_loop = true;
 $_smarty_tpl->tpl_vars['manufacturer']->iteration++;
 $_smarty_tpl->tpl_vars['manufacturer']->index++;
 $_smarty_tpl->tpl_vars['manufacturer']->first = $_smarty_tpl->tpl_vars['manufacturer']->index === 0;
 $_smarty_tpl->tpl_vars['manufacturer']->last = $_smarty_tpl->tpl_vars['manufacturer']->iteration === $_smarty_tpl->tpl_vars['manufacturer']->total;
?>
                                                <li class="top_manufacturer <?php if ($_smarty_tpl->tpl_vars['manufacturer']->first){?>first<?php }elseif($_smarty_tpl->tpl_vars['manufacturer']->last){?>last<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getmanufacturerLink($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer'],$_smarty_tpl->tpl_vars['manufacturer']->value['link_rewrite']);?>
" <?php echo $_smarty_tpl->tpl_vars['manufacturer']->value['name'];?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['manufacturer']->value['name'], 'htmlall', 'UTF-8');?>
</a></li>
                                            <?php } ?>

                                        <?php }elseif(isset($_smarty_tpl->tpl_vars['positions']->value['top']['top_suppliers'])){?>
                                            <span class="title"><?php echo smartyTranslate(array('s'=>'Suppliers','mod'=>'autumnmegamenu'),$_smarty_tpl);?>
</span>
                                            <?php  $_smarty_tpl->tpl_vars['supplier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['supplier']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['top']['top_suppliers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['supplier']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['supplier']->iteration=0;
 $_smarty_tpl->tpl_vars['supplier']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['supplier']->key => $_smarty_tpl->tpl_vars['supplier']->value){
$_smarty_tpl->tpl_vars['supplier']->_loop = true;
 $_smarty_tpl->tpl_vars['supplier']->iteration++;
 $_smarty_tpl->tpl_vars['supplier']->index++;
 $_smarty_tpl->tpl_vars['supplier']->first = $_smarty_tpl->tpl_vars['supplier']->index === 0;
 $_smarty_tpl->tpl_vars['supplier']->last = $_smarty_tpl->tpl_vars['supplier']->iteration === $_smarty_tpl->tpl_vars['supplier']->total;
?>
                                                <li class="top_supplier <?php if ($_smarty_tpl->tpl_vars['supplier']->first){?>first<?php }elseif($_smarty_tpl->tpl_vars['supplier']->last){?>last<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getsupplierLink($_smarty_tpl->tpl_vars['supplier']->value['id_supplier'],$_smarty_tpl->tpl_vars['supplier']->value['link_rewrite']);?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['supplier']->value['name'], 'htmlall', 'UTF-8');?>
</a></li>
                                            <?php } ?>

                                        <?php }?>
                                    </ul>
                                </div>
                            <?php }?>


                            <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['m_left'])&&!isset($_smarty_tpl->tpl_vars['positions']->value['m_left']['HIDE'])){?>
                                <div id="megamenu_left">

                                    <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['m_left']['CAT'])){?>
                                        <ul>
                                           <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['m_left']['CAT']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['category']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['category']->iteration=0;
 $_smarty_tpl->tpl_vars['category']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['category']->iteration++;
 $_smarty_tpl->tpl_vars['category']->index++;
 $_smarty_tpl->tpl_vars['category']->first = $_smarty_tpl->tpl_vars['category']->index === 0;
 $_smarty_tpl->tpl_vars['category']->last = $_smarty_tpl->tpl_vars['category']->iteration === $_smarty_tpl->tpl_vars['category']->total;
?>
                                               <li class="category_title <?php if (($_smarty_tpl->tpl_vars['category']->first&&$_smarty_tpl->tpl_vars['category']->total>1)){?>first<?php }elseif(($_smarty_tpl->tpl_vars['category']->last&&$_smarty_tpl->tpl_vars['category']->total>1)){?>last<?php }elseif($_smarty_tpl->tpl_vars['category']->total==1){?>one<?php }?>">
                                                   <a href="<?php echo $_smarty_tpl->tpl_vars['category']->value['link'];?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['category']->value['name'], 'htmlall', 'UTF-8');?>
</a>

                                               <?php if (isset($_smarty_tpl->tpl_vars['category']->value['children'])){?>
                                                   <ul class="category_childlist">
                                                        <ol class="category_sublist">
                                                            <?php  $_smarty_tpl->tpl_vars['childName'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['childName']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['category']->value['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['childName']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['childName']->key => $_smarty_tpl->tpl_vars['childName']->value){
$_smarty_tpl->tpl_vars['childName']->_loop = true;
 $_smarty_tpl->tpl_vars['childName']->iteration++;
?>

                                                                <li class="category_child"><a href="<?php echo $_smarty_tpl->tpl_vars['childName']->value['link'];?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['childName']->value['name'], 'htmlall', 'UTF-8');?>
</a></li>

                                                                 <?php if (isset($_smarty_tpl->tpl_vars['childName']->value['children'])){?>
                                                                     <ul class="childstep2">
                                                                         <?php  $_smarty_tpl->tpl_vars['childStep2Link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['childStep2Link']->_loop = false;
 $_smarty_tpl->tpl_vars['childStep2Name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['childName']->value['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['childStep2Link']->key => $_smarty_tpl->tpl_vars['childStep2Link']->value){
$_smarty_tpl->tpl_vars['childStep2Link']->_loop = true;
 $_smarty_tpl->tpl_vars['childStep2Name']->value = $_smarty_tpl->tpl_vars['childStep2Link']->key;
?>
                                                                             <li class="category_child"><a href="<?php echo $_smarty_tpl->tpl_vars['childStep2Link']->value;?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['childStep2Name']->value, 'htmlall', 'UTF-8');?>
</a></li>
                                                                         <?php } ?>
                                                                     </ul>
                                                                 <?php }?>

                                                               <?php if (!($_smarty_tpl->tpl_vars['childName']->iteration % 10)){?>
                                                                    </ol>
                                                                    <ol class="category_sublist">
                                                                <?php }?>  

                                                            <?php } ?>
                                                         </ol>
                                                   </ul>
                                               <?php }?>

                                               </li>
                                           <?php } ?>
                                        </ul>

                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['positions']->value['m_left']['PRD'])){?>
                                        <ul>
                                           <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['m_left']['PRD']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['product']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['product']->iteration=0;
 $_smarty_tpl->tpl_vars['product']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['product']->iteration++;
 $_smarty_tpl->tpl_vars['product']->index++;
 $_smarty_tpl->tpl_vars['product']->first = $_smarty_tpl->tpl_vars['product']->index === 0;
 $_smarty_tpl->tpl_vars['product']->last = $_smarty_tpl->tpl_vars['product']->iteration === $_smarty_tpl->tpl_vars['product']->total;
?>
                                                   <li class="m_left_product <?php if ($_smarty_tpl->tpl_vars['product']->first){?>first<?php }elseif($_smarty_tpl->tpl_vars['product']->last){?>last<?php }?>" >
                                                       <div class="m_left_prd_img_wrapper">
                                                           <a href="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['link'], 'htmlall', 'UTF-8');?>
" class="m_left_prd_img_link" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>
">
                                                               <img class="m_left_prd_img" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['image_id'],isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_m_default' : 'medium_default');?>
" />
                                                           </a>
                                                       </div>
                                                       <div class="m_left_prd_name">
                                                           <a href="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['link'], 'htmlall', 'UTF-8');?>
" class="m_left_prd_img_link" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>
">
                                                              <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>

                                                           </a>
                                                       </div>
                                                   </li>
                                           <?php } ?>
                                        </ul>


                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['positions']->value['m_left']['CMSL'])){?>
                                       <ul>
                                           <?php  $_smarty_tpl->tpl_vars['cmsl_link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cmsl_link']->_loop = false;
 $_smarty_tpl->tpl_vars['cmsl_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['m_left']['CMSL']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cmsl_link']->key => $_smarty_tpl->tpl_vars['cmsl_link']->value){
$_smarty_tpl->tpl_vars['cmsl_link']->_loop = true;
 $_smarty_tpl->tpl_vars['cmsl_name']->value = $_smarty_tpl->tpl_vars['cmsl_link']->key;
?>
                                                   <li class="m_left_cmsl">
                                                      <a href="<?php echo $_smarty_tpl->tpl_vars['cmsl_link']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['cmsl_name']->value;?>
</a>
                                                   </li>
                                           <?php } ?>
                                       </ul>


                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['positions']->value['m_left']['CMSP'])){?>
                                       <?php  $_smarty_tpl->tpl_vars['cmsp_content'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cmsp_content']->_loop = false;
 $_smarty_tpl->tpl_vars['cmsp_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['m_left']['CMSP']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cmsp_content']->key => $_smarty_tpl->tpl_vars['cmsp_content']->value){
$_smarty_tpl->tpl_vars['cmsp_content']->_loop = true;
 $_smarty_tpl->tpl_vars['cmsp_id']->value = $_smarty_tpl->tpl_vars['cmsp_content']->key;
?>
                                           <div class="m_left_cmsp">
                                               <?php echo $_smarty_tpl->tpl_vars['cmsp_content']->value;?>

                                           </div>
                                       <?php } ?>  


                                    <?php }?>

                                 </div>
                            <?php }?>

                            <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['bottom'])&&!isset($_smarty_tpl->tpl_vars['positions']->value['bottom']['bottom_hide'])){?>
                                <div id="megamenu_bottom">
                                    <ul>
                                        <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['bottom']['bottom_manufacturers'])){?>
                                            <span class="title"><?php echo smartyTranslate(array('s'=>'Manufacturers','mod'=>'autumnmegamenu'),$_smarty_tpl);?>
</span>
                                            <?php  $_smarty_tpl->tpl_vars['manufacturer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['manufacturer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['bottom']['bottom_manufacturers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['manufacturer']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['manufacturer']->iteration=0;
 $_smarty_tpl->tpl_vars['manufacturer']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['manufacturer']->key => $_smarty_tpl->tpl_vars['manufacturer']->value){
$_smarty_tpl->tpl_vars['manufacturer']->_loop = true;
 $_smarty_tpl->tpl_vars['manufacturer']->iteration++;
 $_smarty_tpl->tpl_vars['manufacturer']->index++;
 $_smarty_tpl->tpl_vars['manufacturer']->first = $_smarty_tpl->tpl_vars['manufacturer']->index === 0;
 $_smarty_tpl->tpl_vars['manufacturer']->last = $_smarty_tpl->tpl_vars['manufacturer']->iteration === $_smarty_tpl->tpl_vars['manufacturer']->total;
?>
                                                <li class="bottom_manufacturer <?php if ($_smarty_tpl->tpl_vars['manufacturer']->first){?>first<?php }elseif($_smarty_tpl->tpl_vars['manufacturer']->last){?>last<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getmanufacturerLink($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer'],$_smarty_tpl->tpl_vars['manufacturer']->value['link_rewrite']);?>
" <?php echo $_smarty_tpl->tpl_vars['manufacturer']->value['name'];?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['manufacturer']->value['name'], 'htmlall', 'UTF-8');?>
</a></li>
                                            <?php } ?>

                                        <?php }elseif(isset($_smarty_tpl->tpl_vars['positions']->value['bottom']['bottom_suppliers'])){?>
                                            <span class="title"><?php echo smartyTranslate(array('s'=>'Suppliers','mod'=>'autumnmegamenu'),$_smarty_tpl);?>
</span>
                                            <?php  $_smarty_tpl->tpl_vars['supplier'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['supplier']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['bottom']['bottom_suppliers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['supplier']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['supplier']->iteration=0;
 $_smarty_tpl->tpl_vars['supplier']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['supplier']->key => $_smarty_tpl->tpl_vars['supplier']->value){
$_smarty_tpl->tpl_vars['supplier']->_loop = true;
 $_smarty_tpl->tpl_vars['supplier']->iteration++;
 $_smarty_tpl->tpl_vars['supplier']->index++;
 $_smarty_tpl->tpl_vars['supplier']->first = $_smarty_tpl->tpl_vars['supplier']->index === 0;
 $_smarty_tpl->tpl_vars['supplier']->last = $_smarty_tpl->tpl_vars['supplier']->iteration === $_smarty_tpl->tpl_vars['supplier']->total;
?>
                                                <li class="bottom_supplier <?php if ($_smarty_tpl->tpl_vars['supplier']->first){?>first<?php }elseif($_smarty_tpl->tpl_vars['supplier']->last){?>last<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getsupplierLink($_smarty_tpl->tpl_vars['supplier']->value['id_supplier'],$_smarty_tpl->tpl_vars['supplier']->value['link_rewrite']);?>
"><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['supplier']->value['name'], 'htmlall', 'UTF-8');?>
</a></li>
                                            <?php } ?>

                                        <?php }?>
                                    </ul>
                                </div>
                            <?php }?>
                         </div>
                         
                                                                       
                        <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['m_right'])&&!isset($_smarty_tpl->tpl_vars['positions']->value['m_right']['HIDE'])){?>
                            <div class="right_col">
                                <div id="megamenu_right">

                                    <?php if (isset($_smarty_tpl->tpl_vars['positions']->value['m_right']['RNDF'])){?>
                                        <ul>
                                            <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['m_right']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['product']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['product']->iteration=0;
 $_smarty_tpl->tpl_vars['product']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['product']->key;
 $_smarty_tpl->tpl_vars['product']->iteration++;
 $_smarty_tpl->tpl_vars['product']->index++;
 $_smarty_tpl->tpl_vars['product']->first = $_smarty_tpl->tpl_vars['product']->index === 0;
 $_smarty_tpl->tpl_vars['product']->last = $_smarty_tpl->tpl_vars['product']->iteration === $_smarty_tpl->tpl_vars['product']->total;
?>
                                                    <li class="m_right_random_prd" >
                                                        <span class="title"><?php echo smartyTranslate(array('s'=>'Random Featured','mod'=>'autumnmegamenu'),$_smarty_tpl);?>
</span>
                                                        <div class="m_right_prd_img_wrapper">
                                                            <a href="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['link'], 'htmlall', 'UTF-8');?>
" class="m_right_prd_img_link" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>
">
                                                                <img class="m_right_prd_img" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['image_id'],isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_default' : 'home_default');?>
" />
                                                            </a>
                                                        </div>
                                                        <div class="m_right_prd_name">
                                                            <a href="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['link'], 'htmlall', 'UTF-8');?>
" class="m_right_prd_img_link" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>
">
                                                               <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>

                                                            </a>
                                                        </div>
                                                    </li>
                                            <?php } ?>
                                        </ul>


                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['positions']->value['m_right']['RND'])){?>
                                        <ul>
                                            <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['positions']->value['m_right']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['product']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['product']->iteration=0;
 $_smarty_tpl->tpl_vars['product']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
$_smarty_tpl->tpl_vars['product']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['product']->key;
 $_smarty_tpl->tpl_vars['product']->iteration++;
 $_smarty_tpl->tpl_vars['product']->index++;
 $_smarty_tpl->tpl_vars['product']->first = $_smarty_tpl->tpl_vars['product']->index === 0;
 $_smarty_tpl->tpl_vars['product']->last = $_smarty_tpl->tpl_vars['product']->iteration === $_smarty_tpl->tpl_vars['product']->total;
?>
                                                    <li class="m_right_random_prd" >
                                                        <span class="title"><?php echo smartyTranslate(array('s'=>'Random Special','mod'=>'autumnmegamenu'),$_smarty_tpl);?>
</span>
                                                        <div class="m_right_prd_img_wrapper">
                                                            <a href="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['link'], 'htmlall', 'UTF-8');?>
" class="m_right_prd_img_link" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>
">
                                                                <img class="m_right_prd_img" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['product']->value['image_id'],isset($_smarty_tpl->tpl_vars['image_shape']->value)&&$_smarty_tpl->tpl_vars['image_shape']->value=='rect_img' ? 'rect_default' : 'home_default');?>
" />
                                                            </a>
                                                        </div>
                                                        <div class="m_right_prd_name">
                                                            <a href="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['link'], 'htmlall', 'UTF-8');?>
" class="m_right_prd_img_link" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>
">
                                                               <?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['product']->value['name'], 'htmlall', 'UTF-8');?>

                                                            </a>
                                                        </div>
                                                    </li>
                                            <?php } ?>
                                        </ul>


                                    <?php }elseif(isset($_smarty_tpl->tpl_vars['positions']->value['m_right']['IMG'])){?>
                                        <div class="m_right_image">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['positions']->value['m_right']['IMG'];?>
" />
                                        </div>


                                    <?php }?>

                                 </div>
                            </div>
                        <?php }?>
                        
                        
                    </div>
            
                <?php }?>
             
             <?php }?>
                            
        </li>
    
<?php } ?>
 
</ul>

<div id="megamenu-responsive">
    <ul id="megamenu-responsive-root">
        <li class="menu-toggle"><p></p><?php echo smartyTranslate(array('s'=>'Navigation','mod'=>'autumnmegamenu'),$_smarty_tpl);?>
</li>
        <li class="root">
            <?php echo $_smarty_tpl->tpl_vars['responsive_menu']->value;?>

        </li>
        
        
        <!-- STARTING OF AN EXAMPLE CODES FOR CUSTOM LINKS IN MOBILE MENU -- (DELETE THIS LINE TO ACTIVATE THEM)
        
        <li class="root">
            <ul>
                <li>
                    
                    <ul>
                        <li><a href="#" title=""><span>Extra Menu Item</span></a></li>
                        <li><a href="#" title=""><span>Extra Menu Item 2</span></a></li>

                        <li class="parent">
                            <a href="#" title=""><span>Extra Menu Item With Submenus</span></a>
                            <ul>
                                <li><a href="#" title=""><span>Submenu 1</span></a></li>
                                <li><a href="#" title=""><span>Submenu 2<span></a></li>
                            </ul>
                        </li>
                    </ul>
                    
                </li>
            </ul>
        </li>
        
        ENDING OF AN EXAMPLE CODES FOR CUSTOM LINKS IN MOBILE MENU -- (DELETE THIS LINE TO ACTIVATE THEM) -->
        
    </ul>
</div>
<?php }} ?>