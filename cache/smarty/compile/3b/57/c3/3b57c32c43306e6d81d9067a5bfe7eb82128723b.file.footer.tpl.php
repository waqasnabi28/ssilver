<?php /* Smarty version Smarty-3.1.14, created on 2013-10-25 07:12:35
         compiled from "D:\xampp\htdocs\ssilver\themes\autumn\footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19527526a52238a0432-50885842%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b57c32c43306e6d81d9067a5bfe7eb82128723b' => 
    array (
      0 => 'D:\\xampp\\htdocs\\ssilver\\themes\\autumn\\footer.tpl',
      1 => 1382697938,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19527526a52238a0432-50885842',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content_only' => 0,
    'HOOK_LEFT_COLUMN' => 0,
    'socialLinks' => 0,
    'customSocialLinks' => 0,
    'socialLink' => 0,
    'customSocialLink' => 0,
    'img_dir' => 0,
    'HOOK_FOOTER' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_526a52239e97f3_53625235',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_526a52239e97f3_53625235')) {function content_526a52239e97f3_53625235($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include 'D:\\xampp\\htdocs\\ssilver\\tools\\smarty\\plugins\\modifier.capitalize.php';
if (!is_callable('smarty_modifier_date_format')) include 'D:\\xampp\\htdocs\\ssilver\\tools\\smarty\\plugins\\modifier.date_format.php';
?>

<?php if (!$_smarty_tpl->tpl_vars['content_only']->value){?>
    </div> <!-- /column -->

    <?php if ($_smarty_tpl->tpl_vars['HOOK_LEFT_COLUMN']->value){?>
        <div id="left-column" class="column one-third"> <?php echo $_smarty_tpl->tpl_vars['HOOK_LEFT_COLUMN']->value;?>
 </div>
    <?php }?>

    </div> <!-- content container -->
    </div> <!-- content-fluid -->
    <div id="footer-fluid" class="fluid-bg">

        <div id="footer-top" class="fluid-bg">
            <div class="container">

                <?php if (isset($_smarty_tpl->tpl_vars['socialLinks']->value)&&$_smarty_tpl->tpl_vars['socialLinks']->value||isset($_smarty_tpl->tpl_vars['customSocialLinks']->value)&&$_smarty_tpl->tpl_vars['customSocialLinks']->value){?>
                    <div id="footer-top-content" class="column full">
                        <p class="social-link-info"><?php echo smartyTranslate(array('s'=>'Connect with us:'),$_smarty_tpl);?>
 </p>

                        <?php if (isset($_smarty_tpl->tpl_vars['socialLinks']->value)&&$_smarty_tpl->tpl_vars['socialLinks']->value){?>
                            <?php  $_smarty_tpl->tpl_vars['socialLink'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['socialLink']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['socialLinks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['socialLink']->key => $_smarty_tpl->tpl_vars['socialLink']->value){
$_smarty_tpl->tpl_vars['socialLink']->_loop = true;
?>
                                <a class="social-link" title="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['socialLink']->key);?>
" href="<?php echo $_smarty_tpl->tpl_vars['socialLink']->value;?>
">
                                    <div class="social <?php echo $_smarty_tpl->tpl_vars['socialLink']->key;?>
"></div>
                                </a>
                            <?php } ?>
                        <?php }?>

                        <?php if (isset($_smarty_tpl->tpl_vars['customSocialLinks']->value)&&$_smarty_tpl->tpl_vars['customSocialLinks']->value){?>
                            <?php  $_smarty_tpl->tpl_vars['customSocialLink'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['customSocialLink']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['customSocialLinks']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['customSocialLink']->key => $_smarty_tpl->tpl_vars['customSocialLink']->value){
$_smarty_tpl->tpl_vars['customSocialLink']->_loop = true;
?>
                                <a class="custom-social-link" title="<?php echo smarty_modifier_capitalize($_smarty_tpl->tpl_vars['customSocialLink']->value['name']);?>
" href="<?php echo $_smarty_tpl->tpl_vars['customSocialLink']->value['link'];?>
">
                                    <img class="custom-social-link-img" src="<?php echo $_smarty_tpl->tpl_vars['img_dir']->value;?>
/autumn/custom_social_icons/<?php echo $_smarty_tpl->tpl_vars['customSocialLink']->value['name'];?>
.png"/>
                                </a>
                            <?php } ?>
                        <?php }?>

                    </div>
                <?php }?>

            </div>
        </div>

        <div id="footer-center" class="fluid-bg">
            <div class="container">
                <div id="footer" class="column full">
                    <?php echo $_smarty_tpl->tpl_vars['HOOK_FOOTER']->value;?>

                </div>
            </div>
        </div>

        <div id="footer-bottom" class="fluid-bg">
            <div class="container">
                <div id="footer-bottom-content" class="column full">
                    <?php echo smartyTranslate(array('s'=>'AUTUMN STORE © %d.','sprintf'=>smarty_modifier_date_format(time(),'%Y')),$_smarty_tpl);?>
 <?php echo smartyTranslate(array('s'=>'Powered by Prestashop™. All Rights Reserved.'),$_smarty_tpl);?>

                </div>
            </div>
        </div>
    </div>
    </div> <!-- wrapper -->
<?php }?>
</body>
</html>
<?php }} ?>