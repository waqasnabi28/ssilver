{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com> *  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


{if count($options) && isset($options)}
<table cellspacing="0" cellpadding="0" class="table" style="width:28em;">
  <tr>
    <th> <input type="checkbox" name="checkme" id="checkme" class="noborder" onclick="checkDelBoxes(this.form, 'categories[]', this.checked)" />
    </th>
    <th>{l s='ID'}</th>
    <th>{l s='Categories name'}</th>
  </tr>
  {foreach $options.query as $option}
  <tr {if $key %2}class="alt_row"{/if}>
    <td> {assign var=id_checkbox value=categories|cat:'_'|cat:$option['id_category']}
      <input type="checkbox" name="categories[]" class="categories" id="{$id_checkbox}" value="{$option['id_category']}" {foreach $selectedCategories as $selectedCate}
{if $selectedCate['id_category'] == $option['id_category']}
checked="checked"
{/if}

{/foreach} /></td>
    <td>{$option['id_category']}</td>
    <td><label for="{$id_checkbox}" class="t">{$option['name']}</label></td>
  </tr>
  {/foreach}
</table>
{else}
<p>{l s='No group created'}</p>
{/if}
</div>
<div class="clear"></div>
<label>Customer List</label>
<div class="margin-form">
  <table cellspacing="0" cellpadding="0" class="table" style="width:28em;">
    <tbody>
      <tr>
       <th>Customer Manual Number</th>
      </tr>
      {if count($selectedCustomerManualID) && isset($selectedCustomerManualID)}
      {foreach $selectedCustomerManualID as $Manual_ids}
      <tr>
        <td>{$Manual_ids}</td>
      </tr>
       {/foreach} 
       {else}
        <tr>
        <td>Not found any record</td>
      </tr>
       {/if}
    </tbody>
  </table>
  
  

</div>
<div class="clear"></div>
 <div class="margin-form" id="thisid">
 <input type="text" name="customerno[]" style="margin-bottom:5px;"    id="customerno" />
 <input type="button" name="submitCustomer" id="submitCustomer" value="Add customer" style="background-color: #FFF6D3;
border: 1px solid #DFD5AF;
border-left: 1px solid #FFF6D3;
border-top: 1px solid #FFF6D3;
color: #268CCD;
padding: 3px;" />

</div>
<script>
$( document ).ready(function() {
 $('#submitCustomer').click( function(){
	$('#customerno').after('<br /><input type="text" name="customer_number[]" value='+$('#customerno').val()+' style="margin-bottom:5px;"   id="customerno" />');
	
	$('#customerno').val('');
	});
});
</script>
