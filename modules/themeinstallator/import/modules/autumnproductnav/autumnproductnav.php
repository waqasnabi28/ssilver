<?php
/* Autumn Theme - AddThis Module - 2012 - Sercan YEMEN - twitter.com/sercan */
    
if (!defined('_PS_VERSION_'))
	exit;

class AutumnProductNav extends Module{
    
    private $_output = '';
    
    function __construct(){
        $this->name = 'autumnproductnav';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'Sercan YEMEN';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Autumn Theme - Easy Product Navigaton Module');
        $this->description = $this->l('Adds "Next/Previous Product" links in product pages.');
    }
    

/*-------------------------------------------------------------*/
/*  INSTALL THE MODULE
/*-------------------------------------------------------------*/
    
    public function install(){
        if (parent::install() && $this->registerHook('displayHeader')){
            return true;
        }
        return false;
    }
    
    
/*-------------------------------------------------------------*/
/*  UNINSTALL THE MODULE
/*-------------------------------------------------------------*/    
    
    public function uninstall(){
        if (parent::uninstall()){
            return true;
        }
        return false;
    }    
    
    
    
/*-------------------------------------------------------------*/
/*  PREPARE FOR HOOK
/*-------------------------------------------------------------*/          

        private function _prepHook($params){
            
            $id_product = (int)Tools::getValue('id_product');
            $currentProduct = New Product((int)$id_product);
                        
            if (isset($params['category']->id_category)){
                $category = $params['category'];
            }else{
                if (isset($currentProduct->id_category_default) AND $currentProduct->id_category_default > 1){
                    $category = New Category((int)($currentProduct->id_category_default));
                }
            }
            
            if (!Validate::isLoadedObject($category) OR !$category->active) 
                return;
            
            
            // It is a very nasty way to get product ids. Basically it loads all of the products from current category. (Not all, just 500000 of them)
            // If you have -lets say 5000- products in your category, it probably takes forever to load the page.
            // But unfortunately there is no way other than this.
            $categoryProducts = $category->getProducts($this->context->language->id, 1, 500000);
            
            
            $currentKey = NULL;
            foreach ($categoryProducts as $key => $product){
                               
                if ($product['id_product'] == $currentProduct->id){
                    $currentKey = $key;
                                                
                    if (array_key_exists(($currentKey - 1), $categoryProducts)){
                        $previousProduct = array(
                            "id_product" => $categoryProducts[$currentKey - 1]['id_product'],
                            "link_rewrite" => $categoryProducts[$currentKey - 1]['link_rewrite'],
                            "category" => $categoryProducts[$currentKey - 1]['category'],
                            "ean13" => $categoryProducts[$currentKey - 1]['ean13'],
                            "name" => $categoryProducts[$currentKey - 1]['name']
                        );                   
                        $this->smarty->assign('previousProduct', $previousProduct);
                    }else{
                        $noticeForFirstProduct = $this->l('You are in the first product of the category');
                        $this->smarty->assign('noticeForFirstProduct', $noticeForFirstProduct);
                    }
                    
                    if (array_key_exists(($currentKey + 1), $categoryProducts)){
                        $nextProduct = array(
                            "id_product" => $categoryProducts[$currentKey + 1]['id_product'],
                            "link_rewrite" => $categoryProducts[$currentKey + 1]['link_rewrite'],
                            "category" => $categoryProducts[$currentKey + 1]['category'],
                            "ean13" => $categoryProducts[$currentKey + 1]['ean13'],
                            "name" => $categoryProducts[$currentKey + 1]['name'],
                        );      
                        $this->smarty->assign('nextProduct', $nextProduct);
                    }else{
                        $noticeForLastProduct = $this->l('You are in the last product of the category');
                        $this->smarty->assign('noticeForLastProduct', $noticeForLastProduct);
                    }
                
                    break;
                    
                }
            
            }
            $this->context->controller->addCSS(($this->_path).'autumnproductnav.css', 'all');
            $render = $this->display(__FILE__, 'autumnproductnav.tpl');
            
            $this->smarty->assignGlobal('autumnproductnav', $render);
            
        }
        
        
/*-------------------------------------------------------------*/
/*  HOOK (displayHeader)
/*-------------------------------------------------------------*/
        
        public function hookDisplayHeader ($params){
            //Check if the user in product page
            $id_product = Tools::getValue('id_product');
            
            if (!$id_product){
                return false;
            }
            else{
                $this->_prepHook($params);
            }
        }
        
}