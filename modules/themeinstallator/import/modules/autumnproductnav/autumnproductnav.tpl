<div id="easyproductnav">
    
    <div class="previous_product">
        {if isset($previousProduct) && $previousProduct}
            
            <a class="autumn-button" href="{$link->getProductLink($previousProduct.id_product, $previousProduct.link_rewrite, $previousProduct.category, $previousProduct.ean13)}">
                <span class="pre_text">&laquo; </span><span class="previous_product_name">{$previousProduct.name}</span>
            </a>
            
        {elseif isset($noticeForFirstProduct) && $noticeForFirstProduct}
            
            <span class="first_product_notice">{$noticeForFirstProduct}</span>
            
        {/if}
    </div>
    
    <div class="next_product">
        {if isset($nextProduct) && $nextProduct}
            
            <a class="autumn-button" href="{$link->getProductLink($nextProduct.id_product, $nextProduct.link_rewrite, $nextProduct.category, $nextProduct.ean13)}">
               <span class="next_product_name">{$nextProduct.name}</span> <span class="pre_text"> &raquo;</span>
            </a>
            
        {elseif isset($noticeForLastProduct) && $noticeForLastProduct}
            
            <span class="last_product_notice">{$noticeForLastProduct}</span>
            
        {/if}
    </div>
    
</div>