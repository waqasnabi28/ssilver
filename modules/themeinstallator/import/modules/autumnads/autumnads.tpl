{* Autumn Theme - Frontpage Ads Module - 2012 - Sercan YEMEN - twitter.com/sercan *}

<div id="autumn-ads" class="group grid-container">
    <ul class="grid">
        {foreach $ads as $ad}

            <li class="item group ad">
                {if ($ad.url) != ""}<a href="{$ad.url}">{/if}
                    <img class="autumn-ads-ad-image" src="{$modules_dir}autumnads/ads/{$ad.image}"/>
                {if ($ad.url) != ""}</a>{/if}
            </li>

        {/foreach}
    </ul>
</div>