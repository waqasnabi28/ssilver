<?php
/*
 *  Prestashop (http://prestashop.com)
 *  
 *  School Order Number Module - Allows currently running stores to
 *  	accept payments by school order number and send a 30-day invoice
 *
 * 	This module was adapted from Kevin Klika's Credit Card module.
 */
 
class School extends PaymentModule
{	
	private $_html = '';
	private $_postErrors = array();
	public  $currencies, $bf, $path;

	function __construct()
	{
		$this->name = 'school';
		$this->displayName = 'school';
		$this->tab = 'Payment';
		$this->version = 2.05;
		
		$this->idOrderState = Configuration::get('SCHOOL_ID_ORDER_STATE');
		$this->curVersionFileURL = 'http://Ox40.us/cc_Current.txt';
		$this->bf = new Blowfish(_COOKIE_KEY_, _COOKIE_IV_);			//Create Blowfish Object for Encryption

		if (!Configuration::get('SCHOOL_CURRENCIES'))				//If, for some reason, there are no currencies, make them
			$this->_makeCurrencies();
		
		$config = Configuration::get('SCHOOL_CURRENCIES');
		$this->currencies 	= isset($config['SCHOOL_CURRENCIES']) ? $config['SCHOOL_CARD_CURRENCIES'] : null;

		parent::__construct();

		/* The parent construct is required for translations */
		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Purchase Order Number');
		$this->description = $this->l('Accept payment by using purchase order number and 30-day invoice');
		$this->path = $this->_path;
	}

	/**
	*	install()
	*	Called when 'Install' is clicked on module
	*/
	function install()
	{
		if(parent::install())
		{
			$this->registerHook('payment');				//Register Payment Hooks with Prestashop
			$this->registerHook('paymentReturn');
			$this->registerHook('invoice');
			
			$this->_makeCurrencies();					//Create Default Currencies
			
			$this->_makeOrderState();					//Create Credit Card Order State
					
			/**
			*	Create the table to store credit card data
			*/
			$db = Db::getInstance();
			$result = $db->Execute("show table status like `"._DB_PREFIX_."`");
			if($result == "")
			{
				$query = "CREATE TABLE `"._DB_PREFIX_."order_school` (
					`id_record` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
					`id_order` INT NOT NULL ,
					`data_string` TEXT NOT NULL ,
					`date_add` DATE ,
					`date_upd` DATE
					) ENGINE = MYISAM ";
				$db->Execute($query);
			}
			
			return true;
		}
		else
			return false;
	}

	function uninstall()
	{
		Configuration::deleteByName('SCHOOL_CURRENCIES');
		return parent::uninstall();
	}

	/**
	*	getContent()
	*	Called in Back Office when user clicks "Configure"
	*/
	function getContent()
	{
		$this->_html = '<h2>'.$this->displayName.'</h2>';		//Display Header
		
		if (!empty($_POST)){
			$this->_postValidation();
			if (!sizeof($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors AS $err)
					$this->_html .= "<div class='alert error'>{$err}</div>";
		} else
			$this->_html .= "<br />";

		$this->_displaySchool();
		$this->_displayForm();

		return $this->_html;
	}

	/**
	*	execPayment($cart)
	*	Called from payment.php
	*/
	function execPayment($cart)
	{
		$errNumber = '0';
//		echo "<pre>"; print_r($_POST);echo "</pre>";

		if(isset($_POST['paymentSubmit']))
		{
        	$orderNumber = $_POST['orderNumber'];

			if (trim($orderNumber) == "")
				$errNumber = '1';
		
			if ($errNumber == '1')
			{
				global $cookie, $smarty;
		
				$currencies = Currency::getCurrencies();
				$authorized_currencies = array_flip(explode(',', $this->currencies));
				$currencies_used = array();
				foreach ($currencies as $key => $currency)
					if (isset($authorized_currencies[$currency['id_currency']]))
						$currencies_used[] = $currencies[$key];
							
					$smarty->assign(array(
						'errNumber'			=> $errNumber,
						'orderNumber'		=> $cardNumber,
						'currency_default' 	=> new Currency(Configuration::get('PS_CURRENCY_DEFAULT')),
						'currencies' 		=> $currencies_used,
						'total' 			=> number_format($cart->getOrderTotal(true, 3), 2, '.', ''),
						'this_path' 		=> $this->_path,
						'this_path_ssl' 	=> Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"));
		
				return $this->display(__FILE__, 'payment_execution.tpl');
			}
			else
			{
                            global $cookie, $smarty;
                                        /* Create Necessary variables for order placement */	
					$cardString = "Order Number: {$orderNumber} <br/>";
                                        $cardString = pSQL($this->bf->encrypt($cardString), true);
										
					/*check for duplicate order number*/
                                        $count = $this->checkforduplicate($cardString);
                                        if($count==0){
                                            $currency = new Currency(intval(isset($_POST['currency_payement']) ? $_POST['currency_payement'] : $cookie->id_currency));
                                            $total = floatval(number_format($cart->getOrderTotal(true, 3), 2, '.', ''));
                                            $this->validateOrder($cart->id, $this->idOrderState, $total, $this->displayName, null, $currency->id);					
                                            
                                            /* Insert Credit Card String into the database for the current order.*/
                                            $order = new Order($this->currentOrder);
                                            $order_fields_1 = $order_fields_2 = $order_fields = array();
                                            $manual_customer_id=$this->getManualCustomerId($cookie->id_customer);
                                            $order_fields_1 = array (
                                                array('OrdNum', 'OrdTyp', 'CompNbr', 'CustNum', 'WhsID','CustPurchOrdNum','OrdSrc','EntDate'),
                                                array($order->id, 'O', '1',$manual_customer_id,'1',$orderNumber,'W',date('m/d/y')),
                                            );
                                            $order_fields[] = array("H",$order->id, 'O', '1',$manual_customer_id,'1',$orderNumber,'WB',date('m/d/y'));
                                            
                                            
                                            $order_fields_2[0] = array('OrdNum', 'OrdSeqNum', 'LineItemTyp', 'ItemNum', 'QtyOrd','UntMeas','','');
                                            $products_in_cart = $cart->getProducts();
                                            if(is_array($products_in_cart)){
                                                $i=1;
                                                foreach($products_in_cart as $cartp){
                                                    $order_fields_2[$i] = array($order->id,$i,'I',$cartp['attributes_small'],$cartp['cart_quantity'],'','','');
													$order_fields[] = array("D",$order->id,$i,'I',$cartp['attributes_small'],$cartp['cart_quantity'],'','','');
                                                    $i++;
													
                                                }
                                            }
                                            $this->generate_csv($order->id,$order_fields_1,$order_fields_2,$order_fields);
                                            $this->addDataString($order->id, $cardString);
                                        
                                            /* Once complete, redirect to order-confirmation.php */	
                                            Tools::redirectLink(__PS_BASE_URI__."order-confirmation.php?id_cart={$cart->id}&id_module={$this->id}&id_order={$this->currentOrder}&key={$order->secure_key}");
                                        }else{
                                            
                                            $currencies = Currency::getCurrencies();
                                            $authorized_currencies = array_flip(explode(',', $this->currencies));
                                            $currencies_used = array();

                    //			echo "<pre>DATA: ";print_r($currencies);echo "</pre>";

                                            foreach ($currencies as $key => $currency)
			
                                            if (isset($authorized_currencies[$currency['id_currency']]))
                                            {
            //					echo "IS AUTHORIZED";
                                                    $currencies_used[] = $currencies[$key];
                                            }
                                            $smarty->assign(array(
                                                'currency_default' 	=> new Currency(Configuration::get('PS_CURRENCY_DEFAULT')),
                                                'currencies' 		=> $currencies_used,
                                                'total' 			=> number_format($cart->getOrderTotal(true, 3), 2, '.', ''),
                                                'this_path' 		=> $this->_path,
                                                'this_path_ssl' 	=> (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'modules/'.$this->name.'/'));
                                            $smarty->assign('error','duplicate');
                                            return $this->display(__FILE__, 'payment_execution.tpl');
                                        }

			}
		}
		else
		{
//			echo "ELSE";
			global $cookie, $smarty;
	
			$currencies = Currency::getCurrencies();
			$authorized_currencies = array_flip(explode(',', $this->currencies));
			$currencies_used = array();
			
//			echo "<pre>DATA: ";print_r($currencies);echo "</pre>";
			
			foreach ($currencies as $key => $currency)
			
				if (isset($authorized_currencies[$currency['id_currency']]))
				{
//					echo "IS AUTHORIZED";
					$currencies_used[] = $currencies[$key];
				}

$smarty->assign(array(
	'currency_default' 	=> new Currency(Configuration::get('PS_CURRENCY_DEFAULT')),
	'currencies' 		=> $currencies_used,
	'total' 			=> number_format($cart->getOrderTotal(true, 3), 2, '.', ''),
	'this_path' 		=> $this->_path,
	'this_path_ssl' 	=> (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'modules/'.$this->name.'/'));
	
//	echo "DISPLAY TEMPLATE: ".dirname(__FILE__).'/payment_execution.tpl';
	
			//self::$smarty->display(dirname(__FILE__).'/payment_execution.tpl');
			return $this->display(__FILE__, 'payment_execution.tpl');
		}
	}

	/**
	*	hookPayment($params)
	*	Called in Front Office at Payment Screen
	*/
	function hookPayment($params)
	{
		global $smarty;
		
		$smarty->assign(array(
            'this_path' 		=> $this->_path,
            'this_path_ssl' 	=> Configuration::get('PS_FO_PROTOCOL').$_SERVER['HTTP_HOST'].__PS_BASE_URI__."modules/{$this->name}/"));
			
		return $this->display(__FILE__, 'payment.tpl');
	}
	
	/**
	*	hookInvoice($params)
	*	Called in Back Office upon Order Review
	*	Note: Only return data if an order is a Credit Card order.
	*/
	function hookInvoice($params)
	{
		$id_order = $params['id_order'];
		if($this->isCreditCardOrder($id_order))		
		{
			if(intval($_GET['remData']) == 1)
				$this->removeDataString($id_order);
				
			global $smarty;
			$data_string = $this->getDataString($id_order);
			$smarty->assign(array(
				'ccDataString' 		=> $this->bf->decrypt($data_string),
				'id_order'			=> $id_order,
				'this_page'			=> $_SERVER['REQUEST_URI'],
				'this_path' 		=> $this->_path,
				'this_path_ssl' 	=> (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').__PS_BASE_URI__.'modules/'.$this->name.'/'));

			return $this->display(__FILE__, 'invoice_block.tpl');
		}
		else
			return "";
	}

	/**
	*	hookPaymentReturn($params)
	*	Called in Front Office upon order placement
	*/
	function hookPaymentReturn($params)
	{
		global $smarty;
		
		$state = $params['objOrder']->getCurrentState();
		
		if ($state == _PS_OS_OUTOFSTOCK_ or $state == $this->idOrderState)
			$smarty->assign(array(
				'total_to_pay' 	=> Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false, false),
				'status' 		=> 'ok',
				'id_order' 		=> $params['objOrder']->id
			));
		else
			$smarty->assign('status', 'failed');

		return $this->display(__FILE__, 'payment_return.tpl');
	}

	/**
	*	_postValidation()
	*	Called upon module configuration submit
	*/
	private function _postValidation()
	{
		if (isset($_POST['currenciesSubmit']))
		{
			$currencies = Currency::getCurrencies();
			$authorized_currencies = array();
			foreach ($currencies as $currency)
				if (isset($_POST['currency_'.$currency['id_currency']]) AND $_POST['currency_'.$currency['id_currency']])
					$authorized_currencies[] = $currency['id_currency'];
			if (!sizeof($authorized_currencies))
				$this->_postErrors[] = $this->l('At least one currency is required.');
		}
	}

	/**
	*	_postProcess()
	*	Called upon successful module configuration validation
	*/
	private function _postProcess()
	{
		if (isset($_POST['currenciesSubmit']))
		{
			$currencies = Currency::getCurrencies();
			$authorized_currencies = array();
			foreach ($currencies as $currency)
				if (isset($_POST['currency_'.$currency['id_currency']]) AND $_POST['currency_'.$currency['id_currency']])
					$authorized_currencies[] = $currency['id_currency'];
			Configuration::updateValue('CREDIT_CARD_CURRENCIES', implode(',', $authorized_currencies));
			
			/*Lang Variables*/ $modOk = $this->l('Ok'); $modUpdated = $this->l('Settings Updated Successfully');
			$this->_html .= "<div class='conf confirm'><img src='../img/admin/ok.gif' alt='{$modOk}' />{$modUpdated}</div>";
		}
		else if(isset($_POST['noupdate']))
		{
			Tools::redirectLink('http://Ox40.us/');
		}
		else if(isset($_POST['update']))
		{
			/*Lang Variables*/ $modError = $this->l('Error'); $modNoFunction = $this->l('Function Not Utilized Yet. Updates Must be Performed Manually');
			$this->_html .= "<div class='alert error'><img src='../img/admin/ok.gif' alt='{$modError}' />{$modNoFunction}</div>";
		}
		else if(isset($_POST['paymentSubmit']))
		{
			//asdf
		}
	}

	/**
	*	_displaySchool()
	*	Called in Back Office during Module Configuration
	*/
	private function _displaySchool()
	{
		$modDesc 	= $this->l('This module allows you to accept payments by purchase order number and sending a 30-day invoice.');
		$modStatus	= $this->l('If the client chooses this payment type, the order status will change to \'Awaiting Order Number Validation\'.');
		$modConfirm	= $this->l('Therefore, you will need to manually confirm the order as soon as you receive restitution.');
		$this->_html .= "<img src='../modules/school/invoice.jpg' style='float:left; margin-right:15px;' />
						<b>{$modDesc}</b><br /><br />
						{$modStatus}<br />
						{$modConfirm}<br /><br /><br />";
	}

	/**
	*	_displayForm()
	*	Called in Back Office during Module Configuration
	*/
	private function _displayForm()
	{
		global $smarty;
		$currencies_string = "";
		$currencies = Currency::getCurrencies();
		$authorized_currencies = array_flip(explode(',', Configuration::get('SCHOOL_CURRENCIES')));
		foreach ($currencies as $currency)
		{
				$currencies_string .= '<label style="float:none; "><input type="checkbox" value="true" name="currency_'.$currency['id_currency'].'"'.(isset($authorized_currencies[$currency['id_currency']]) ? ' checked="checked"' : '').' />&nbsp;<span style="font-weight:bold;">'.$currency['name'].'</span> ('.$currency['sign'].')</label><br />';
		}
		$currentVersion = function_exists('file_get_contents') ? file_get_contents($this->curVersionFileURL) : "0.0 Website Unavailable";
		$smarty->assign(array(
				'currencies' => $currencies_string,
				'version' => $this->version,
				'current_version' => $currentVersion));				
        
		$this->_html .= $this->display(__FILE__,'admin_form.tpl');
	}
	
	/**
	*	makeCurrencies()
	*	Called from within creditcard.php if no default currencies exist
	*/
	public function _makeCurrencies()
	{
			$currencies = Currency::getCurrencies();
			$authorized_currencies = array();
			foreach ($currencies as $currency)
				$authorized_currencies[] = $currency['id_currency'];
			return Configuration::updateValue('SCHOOL_CURRENCIES', implode(',', $authorized_currencies));
	}
	
	/**
	*	makeOrderState()
	*	An order state is necessary for this module to function.
	*	The id number of the order state is stored in a global configuration variable for use later
	*/
	private function _makeOrderState()
	{
		if(!(Configuration::get('SCHOOL_ID_ORDER_STATE') > 0))
		{				
			$os = new OrderState();
			$os->name = array_fill(0,10,"Awaiting order number validation");	//Fill with english language translation
			$os->send_mail = 0;
			$os->template = "";
			$os->invoice = 0;
			$os->color = "#33FF99";
			$os->unremovable = false;
			$os->logable = 0;		
			$os->add();
			Configuration::updateValue('SCHOOL_ID_ORDER_STATE',$os->id);
		}
	}
		
	/**
	*	isCreditCardOrder($id_order)
	*	Checks DB to see if an order used a creditcard
	*/
	public function isCreditCardOrder($id_order)
	{
		$db = Db::getInstance();
		$result = $db->getRow('
			SELECT * FROM `'._DB_PREFIX_.'order_school`
			WHERE `id_order` = "'.$id_order.'"');

		return intval($result["id_order"]) != 0 ? true : false;
		
	}
	
	/**
	*	removeDataString($id_order)
	*	Removes credit card data from database upon order completion.
	*/
	public function removeDataString($id_order)
	{
		$removedString = pSQL($this->bf->encrypt("Purchase Order Information Has Been Removed."), true);
		$db = Db::getInstance();
		$result = $db->Execute('
		UPDATE `'._DB_PREFIX_.'order_school`
		SET `data_string` = "'.$removedString.'"
		WHERE `id_order` = "'.intval($id_order).'"');
	}
	
	/**
	*	addDataString($id_order, $data_string)
	*	Adds a data string to the database
	*/
	public function addDataString($id_order, $data_string)
	{
		$db = Db::getInstance();
		$result = $db->Execute('
		INSERT INTO `'._DB_PREFIX_.'order_school`
		( `id_order`, `data_string` )
		VALUES
		("'.intval($id_order).'","'.$data_string.'")');
	}
	
	/**
	*	getDataString($id_order)
	*	Returns the associated credit card string from the database
	*/
	public function getDataString($id_order)
	{
		$db = Db::getInstance();
		$result = $db->ExecuteS('
		SELECT `data_string` FROM `'._DB_PREFIX_.'order_school`
		WHERE `id_order` ="'.intval($id_order).'";');
		return $result[0]['data_string'];
	}
        
        /**
         * check for duplicate order number
         * returns number of duplicate record found
         */
        private function checkforduplicate($string){
            $db = Db::getInstance();
            $result = $db->getRow('SELECT count(1) as num FROM `'._DB_PREFIX_.'order_school`
                                               WHERE data_string like "'.mysql_real_escape_string($string).'"');
            return $result['num'];
        }
        
        private function generate_csv($ordernum,$fields_1,$fields_2,$fields){

                //$orhof_file = "../../../inventory/orders/order_".$ordernum."_orhof.csv";
                //$this->writecsvfile($orhof_file,$fields_1);

                //$ordof_file = "../../../inventory/orders/order_".$ordernum."_ordof.csv";
                //$this->writecsvfile($ordof_file,$fields_2);

				$order_file = "../../inventory/orders/order_".$ordernum.".csv";
                $this->writecsvfile($order_file,$fields);

                
        }
        
        private function writecsvfile($file,$lists){

            if(is_array($lists)){
                $fp = fopen($file, 'w');
                    foreach ($lists as $fields) {
                        fputcsv($fp, $fields);
                    }
                fclose($fp);
            }
             
        }
        
        private function getManualCustomerId($customerid){
            $sql = "SELECT manual_id_customer FROM "._DB_PREFIX_."customer WHERE id_customer = ".$customerid;
           
            $db = Db::getInstance();
            $result = $db->getRow($sql);
            return $result['manual_id_customer'];
            
            
            
        }
}

?>
