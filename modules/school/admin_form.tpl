<div>

<form action="{$smarty.server.REQUEST_URI}" method="post">	

	<fieldset>

	<legend><img src="../img/admin/prefs.gif" />{l s='Version Information'}</legend>

		<table border="0" width="250" cellpadding="0" cellspacing="0">

			<tr>

				<td>

					{l s="Your version is" mod="school"}

				</td>

				<td>

					v{$version}

				</td>

			</tr>	

			<tr>

				<td colspan="2"><br />

						

							{if $version < $current_version}

								<input type="submit" name="update" class="button" value="{l s='Newer Version Available, Click Here to Download Version' mod='school'}{$current_version}" />

							{else}

								<input type="submit" name="noupdate" class="button" value="{l s='No Updates Available, Click Here to Visit the Website' mod='school'}" />

							{/if}

				</td>

			</tr>

		</table>

	</fieldset>

</form>

</div>

<br />

<br />

<form action="{$smarty.server.REQUEST_URI}" method="post">

	<fieldset>

	<legend><img src="../img/t/15.gif" />{l s='Authorized Currencies' mod='school'}</legend>

		<table border="0" width="500" cellpadding="0" cellspacing="0" id="form">

			<tr>

				<td colspan="2">

					{l s='Currencies authorized for school order number payment. Customers will be able to pay using these currencies' mod='school'}

					<br /><br />

				</td>

			</tr>

			<tr>

				<td width="130" style="height: 35px;">{l s='Currencies' mod='school'}</td>

				<td>

					{$currencies}

				</td>

			</tr>

			<tr>

				<td colspan="2" align="center">

					<br />

					<input class="button" name="currenciesSubmit" value="{l s='Update Settings' mod='school'}" type="submit" />

				</td>

			</tr>

		</table>

	</fieldset>

</form>