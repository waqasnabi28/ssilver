{if $status == 'ok'}

	<p>{l s='Your order on' mod='school'} <span class="bold">{$shop_name}</span> {l s='is now complete.' mod='school'}

		<br /><br />

		{l s='Once your purchase order number is verified, your order will be shipped immediately with a 30-day invoice.' mod='school'}

		<br /><br />- {l s='Total Payment Pending:' mod='school'} <span class="price">{$total_to_pay}</span>

		<br /><br />{l s='For any questions or for further information, please contact our' mod='school'} <a href="{$base_dir}contact-form.php">{l s='customer support' mod='school'}</a>.

	</p>

{else}

	<p class="warning">

		{l s='We noticed a problem with your order. If you think this is an error, you can contact our' mod='school'} 

		<a href="{$base_dir}contact-form.php">{l s='customer support' mod='school'}</a>.

	</p>

{/if}

