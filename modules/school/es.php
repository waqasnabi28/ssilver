<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{creditcard}prestashop>admin_form_254f642527b45bc260048e30704edb39'] = 'Configuración';
$_MODULE['<{creditcard}prestashop>admin_form_9d11e23779d9604067dd20e359117573'] = 'Información de versión';
$_MODULE['<{creditcard}prestashop>admin_form_f85e4697c40bb8afb61339ae486005f3'] = 'Nueva versión disponible, haga click aquí para descargar';
$_MODULE['<{creditcard}prestashop>admin_form_dd9540f102669b100fb2655033191c4e'] = 'No hay nueva versión disponible, haga click aquí para ir a la pagina Web';
$_MODULE['<{creditcard}prestashop>admin_form_af4ab2871e183a1fa2cb780b3bff0b46'] = 'Monedas autorizadas';
$_MODULE['<{creditcard}prestashop>admin_form_4c13e534023988603f93c0ac4afbdd29'] = 'Monedas autorizadas para pagos con tarjeta de credito/debito. Los clientes podran pagar con estas monedas';
$_MODULE['<{creditcard}prestashop>admin_form_dfcfc43722eef1eab1e4a12e50a068b1'] = 'Monedas';
$_MODULE['<{creditcard}prestashop>admin_form_61cbc2d26b4157292673c772ddd6c0f7'] = 'Ajustes actualizados';
$_MODULE['<{creditcard}prestashop>creditcard_73a61696f100b3858511e212a3feea6b'] = 'Tarjeta de credito';
$_MODULE['<{creditcard}prestashop>creditcard_ca18d5b4ef040065081f9cc90f95e5a2'] = 'Acepte los pagos fuera de línea con de la tarjeta de crédito';
$_MODULE['<{creditcard}prestashop>creditcard_1818ced69645904b869e2797d895b388'] = 'Por lo menos una moneda se requiere';
$_MODULE['<{creditcard}prestashop>creditcard_a60852f204ed8028c1c58808b746d115'] = 'Ok';
$_MODULE['<{creditcard}prestashop>creditcard_f111976dd81fd9fe176a8740db8e6f1a'] = 'Ajustes actualizados con éxito';
$_MODULE['<{creditcard}prestashop>creditcard_902b0d55fddef6f8d651fe1035b7d4bd'] = 'Error';
$_MODULE['<{creditcard}prestashop>creditcard_4f4554f7f86f8b0a8bb571ff0008afef'] = 'Esta función todavía no esta disponible. Las actualizaciones se deben realizar manualmente';
$_MODULE['<{creditcard}prestashop>creditcard_0611b0384411b250c457e5bb8d2b362f'] = 'Este módulo permite aceptar pagos por tarjeta de crédito/debito.';
$_MODULE['<{creditcard}prestashop>creditcard_9ff429b95433246ca5c06abcba81a7fa'] = 'Si el cliente elige este modo de pago, el estado de la orden cambiará a \' Aguardar Validation\' de la tarjeta de crédito/debito.';
$_MODULE['<{creditcard}prestashop>creditcard_b6f6ac9e8552ccf70df5e1692170f87e'] = 'Por lo tanto, usted necesitará confirmar manualmente la orden tan pronto como usted reciba la restitución.';
$_MODULE['<{creditcard}prestashop>invoice_block_ddf2cbfbba18632d0671ec5fce752ae6'] = 'Información de la tarjeta de crédito/débito';
$_MODULE['<{creditcard}prestashop>invoice_block_b1a7cc34766e2deb1b92e87b3caa1ce0'] = 'Haga click aquí para revelar la información.';
$_MODULE['<{creditcard}prestashop>invoice_block_f048fce3b76cfad3e6c49216be111e3a'] = 'Elimine los datos de la tarjeta de crédito';
$_MODULE['<{creditcard}prestashop>invoice_block_d72faa90e6e97b373e73a731599c589a'] = 'Elimine los datos de la tarjeta de crédito';
$_MODULE['<{creditcard}prestashop>payment_e78050ca4d78b123d1c9a4b171ca7057'] = 'Pague con tarjeta de crédito/débito';
$_MODULE['<{creditcard}prestashop>payment_e78050ca4d78b123d1c9a4b171ca7057'] = 'Pague con tarjeta de crédito/débito';
$_MODULE['<{creditcard}prestashop>payment_e78050ca4d78b123d1c9a4b171ca7057'] = 'Pague con tarjeta de crédito/débito';
$_MODULE['<{creditcard}prestashop>payment_execution_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Envío';
$_MODULE['<{creditcard}prestashop>payment_execution_f1d3b424cd68795ecaa552883759aceb'] = 'Resumen del pedido';
$_MODULE['<{creditcard}prestashop>payment_execution_b27fa2c9836b0e0aca9dd6e1a27ac044'] = 'Pago con tarjeta de crédito/débito';
$_MODULE['<{creditcard}prestashop>payment_execution_d83d5aa5c718fef1f06fb86c00eeb381'] = 'Tarjeta de crédito/débito';
$_MODULE['<{creditcard}prestashop>payment_execution_0d94c82b8ba20294e90b8405d36ed5e5'] = 'Usted ha elegido pagar con tarjeta de crédito/débito.';
$_MODULE['<{creditcard}prestashop>payment_execution_c884ed19483d45970c5bf23a681e2dd2'] = 'Aquí está el resumen de su pedido:';
$_MODULE['<{creditcard}prestashop>payment_execution_e2867a925cba382f1436d1834bb52a1c'] = 'El total de su pedido es';
$_MODULE['<{creditcard}prestashop>payment_execution_26e3f001f655836b64fc31a5af8886da'] = 'Aceptamos varias monedas que se enviarán por medio de tarjeta de crédito';
$_MODULE['<{creditcard}prestashop>payment_execution_a7a08622ee5c8019b57354b99b7693b2'] = 'Elija uno de los siguientes:';
$_MODULE['<{creditcard}prestashop>payment_execution_e929a7d3a143ae341344b85b41f41254'] = 'Aceptamos las siguiente monedas que serán enviadas por tarjeta de crédito:';
$_MODULE['<{creditcard}prestashop>payment_execution_f97d2eb0a66987899d02bb180936afa3'] = 'Error:';
$_MODULE['<{creditcard}prestashop>payment_execution_cc9e698893a39c3c437df5c1f34459f5'] = 'Nombre de la tarjeta de crédito:';
$_MODULE['<{creditcard}prestashop>payment_execution_c7e629b12a4b86a3982a8c1316371fad'] = 'Número de la tarjeta de crédito/débito:';
$_MODULE['<{creditcard}prestashop>payment_execution_ed997956bb3c5fc863e0f98ed7c60f52'] = 'Número CVC de la tarjeta:';
$_MODULE['<{creditcard}prestashop>payment_execution_2d32044287e6a22a8ce2340f7e932c27'] = 'Fecha de expiración:';
$_MODULE['<{creditcard}prestashop>payment_execution_d016df7ef82427b136c614976c082833'] = 'Por favor confirme su pedido haciendo click en \' Confirmo pedido\'';
$_MODULE['<{creditcard}prestashop>payment_execution_569fd05bdafa1712c4f6be5b153b8418'] = 'Otros métodos del pago';
$_MODULE['<{creditcard}prestashop>payment_execution_bb75470ab865932aa0b370b3c387c7b5'] = 'Confirmo pedido';
$_MODULE['<{creditcard}prestashop>payment_return_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Su pedido ';
$_MODULE['<{creditcard}prestashop>payment_return_2c303913046a1db3824ab550c435a302'] = 'se realizo correctamente.';
$_MODULE['<{creditcard}prestashop>payment_return_94fb1e8201ba3cc3f7ff9cd68cab0b8e'] = 'Una vez que se ha verificado su tarjeta y se han aceptado los fondos, su pedido será enviado.';
$_MODULE['<{creditcard}prestashop>payment_return_83ec145f5cf3e2fdbcf5ac2bcb3127fd'] = 'Pago total pendiente:';
$_MODULE['<{creditcard}prestashop>payment_return_0db71da7150c27142eef9d22b843b4a9'] = 'Para cualquier pregunta o para más información, por favor contacte con nuestro';
$_MODULE['<{creditcard}prestashop>payment_return_64430ad2835be8ad60c59e7d44e4b0b1'] = 'Servicio clientes';
$_MODULE['<{creditcard}prestashop>payment_return_8de637e24570c1edb0357826a2ad5aea'] = 'Notamos un problema con su pedido. Si usted piensa que puede tratarse de un error, contacte con nuestro';
$_MODULE['<{creditcard}prestashop>payment_return_64430ad2835be8ad60c59e7d44e4b0b1'] = 'Servicio clientes';

?>
