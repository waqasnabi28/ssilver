{capture name="path"}{l s='Shipping'}{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}

<p><b>{l s='Order summary' mod='school'}</b></p>

{assign var='current_step' value='payment'}

{include file="$tpl_dir./order-steps.tpl"}


{if isset($error)}
    <p class='error'><h3  style='color:red'>{l s='Error: Duplicate Purchase Order Number. Please use a unique Purchase Order Number.' mod='school'}</h3></p>
{/if}
<p>{l s='Purchase Order Payment' mod='school'}</p>

<form action="{$smarty.server.REQUEST_URI}" method="post" id="orderForm" name="orderForm">

<p>

<img src="{$this_path}invoice.jpg" alt="{l s='school 30-day invoice' mod='school'}" style="float:left; margin: 0px 10px 5px 0px;" />

	{l s='You have chosen to pay by purchase order.' mod='school'}

	<br/><br />

	{l s='Here is a short summary of your order:' mod='school'}

</p>

<p style="margin-top:20px;">

	- {l s='The total amount of your order is' mod='school'}

	{if $currencies|@count > 1}

		{foreach from=$currencies item=currency}

			<span id="amount_{$currency.id_currency}" class="price" style="display:none;">{convertPriceWithCurrency price=$total currency=$currency}</span>

		{/foreach}

	{else}

		<span id="amount_{$currencies.0.id_currency}" class="price">{convertPriceWithCurrency price=$total currency=$currencies.0}</span>

	{/if}

</p>

<p>

	-

	{if $currencies|@count > 1}

		{l s='We only accept Purchase Orders at this time.' mod='school'}

		<br /><br />

		{l s='Choose one of the following:' mod='school'}

		<select id="currency_payement" name="currency_payement" onChange="showElemFromSelect('currency_payement', 'amount_')">

		{foreach from=$currencies item=currency}

			<option value="{$currency.id_currency}" {if $currency.id_currency == $currency_default->id}selected="selected"{/if}>{$currency.name}</option>

		{/foreach}

		</select>

		<script language="javascript">showElemFromSelect('currency_payement', 'amount_');</script>

	{else}

		{l s='We only accept purchase orders:' mod='school'}&nbsp;<b>{$currencies.0.name}</b>

		<input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}">

	{/if}

</p>

<p>

	<p>{l s="Order Information" mod="school"}</p>

	<table border="0">

		<tr>

			<td style="vertical-align: bottom;">

				<p>{l s='Purchase Order Number:' mod='school'}</p>

				<div id="errNumber" style="color: red;{if $errNumber eq '1'}display: block;{else}display: none;{/if}">{l s="Valid Order Number is Required" mod="creditcard"}</div>

			</td>

			<td>

				<input type="text" name="orderNumber" id="orderNumber" value="{$orderNumber}" />

			</td>

		</tr>

	</table>

</p>

<p>

	<br /><br />

	<b>{l s='This order will be committed to stock after submission. Please confirm your order by clicking \'Submit Order\'' mod='school'}.</b>

</p>

<p class="cart_navigation">

	<!--<input type="button" onclick="document.location.href='{$base_dir_ssl}order.php?step=3'" value="{l s='Other payment methods' mod='school'}" class="button" style="width:170px !important;"/>-->
    
	<input type="submit" name="paymentSubmit" value="{l s='Submit Order' mod='school'}" class="button" style="float:right; width:120px !important;"/>

</p>

</form>